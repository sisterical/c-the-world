# C the world

**Tools for building cross-platform UIs in C/C++**


## Status

At the current time, C-the-world (or _ctheworld_) is considered a
_work in progress_. It has been used to build published iOS App Store
apps, though only by the primary developer.

If you have questions or ideas, please submit an issue.


## Quick Start

+ `git clone https://github.com/adamtait/ctheworld.git`
+ `./ctheworld/script/tool/new-project.sh app`
+ Open `app/build_ios/app.xcodeproj` in XCode.
+ Follow the [XCode & iOS app tips](developers_guide.md#xcode--ios-apps)
+ Select the _app_ target
+ Run the iOS simulator


### Next steps

+ look at the examples in [examples/](examples/)
+ change `src/cpp/root_node.cpp`
+ Run the iOS simulator, again


## What is "C the world"?

The goal is build a suite of tools for building UIs (User Interfaces)
to run on iOS and Android. We may eventually expand to other platforms
(like web), but they are so far out of scope that they're not being
considered. In fact, iOS is the current focus and Android support is
not being considered.

C-the-World is a suite of components, rather than a single framework,
because I think it's better for the evolution of this work. A
framework implies coupling (for power) and coupling implies rigidity.
Better that the individual components be swapped out than the whole
collection of ideas be disregarded. At the same time, I don't expect
these tools to be everything to everyone.

[Dropbox's simple but powerful Djinni](https://github.com/dropbox/djinni)
tooling is the basis for C-the-World. You could also think of
C-the-World as an easier path to getting started with Djinni, at least
one way of using it.



### Assumptions

Code contained herein can be used/re-used with the following
assumptions:

+ you understand software and the domain of mobile development enough to know the trade-offs in your implementation choices. Choosing the same trade-offs made here may not be in your best interest.
+ you like to make your own choices about implementation. you prefer control and flexibility over a quick start.
+ your app has a low number of animations. instead, it's mostly information/data based. this code might be useful in making games (or highly visual apps) but no support will be provided other than an accepted method to add native extensions
+ if you work on a team, you like teaching others. no teaching materials are planned and documentation may be limited or out of date.



### Caveat

I am building this for myself. I'm happy if other people can re-use my
ideas or work, but that's not my primary goal. I run a business and
its success is my focus.

I may make breaking changes or choose tools/components that don't suit
others. Hopefully, it will be easy enough to fork and go your own
way (and if it's not, please change it and pull request).

That said, I welcome fresh ideas and contributions. 



### Technical or Implementation Details, please!

If you're looking for the details, [read the Architecture Decision
Records (in `/adr`)](adr/). 

0. [ADR Template](adr/000-adr-template.md)
1. [Cross Platform](adr/001-cross-platform.md)
2. [Shared Code](adr/002-shared-code.md)
3. [Immutable Views](adr/003-immutable-views.md)
4. [Events](adr/004-events.md)
5. [Root View Node](adr/005-root-view-node.md)


After that, you might want to look at the example (not yet written).



## Concepts

### DSL for any UI

Another developer once told me that _"every software system I build
is my attempt at reducing the domain into a DSL that makes describing
the challenges easy"_. I've found great success with this approach
(_and further appreciate studies in compilers & interpreters_).

So, building a DSL for UIs is my primary goal. I want to make building
& managing UIs as easy as possible because I plan on creating many of
them.

Building a universal DSL for (modern) user interfaces might seem like
an outrageous goal since there are so many different ways UIs are
implemented. I'll start by constraining to only the platforms that I
care about for my business; iOS first, followed by Android, and
finally web. Furthermore each of those platforms has a different way
of thinking about user interfaces and a different set of
implementation languages, libraries and trade-offs. Again, I'm going
to have to constrain the domain of what's possible to only the
features that I use. For example, I may implement tools for creating
animations but that's a very low priority for me. I don't plan to
unify the complex OpenGL or iOS Metal libraries. However, I plan to
make c-the-world flexible so that you will be able to build your own
extensions or reference your own platform-specific implementations.


  

## Open Questions

Below is the set of questions that I have yet to answer:

+ What's the best way to validate the data structures? Dynamic
assertions are easy to implement but terrible when they fire while
running a release build. A flag or validation phase would keep
assertions from breaking release code, but also require the developer
to configure correctly.
+ How should we allow extensions with new native code?
+ How should we allow definitions of platform specific code?
+ How do we define the root node in a clear way? Including it in
`app_impl.cpp` isn't great, since it's hidden.



## Inspiration

1. [React Native](https://facebook.github.io/react-native/docs/getting-started.html)
2. [AsyncDisplayKit (now, Texture)](https://www.texturegroup.org)
3. [Android XML](https://developer.android.com/guide/topics/ui/declaring-layout)
4. [Flutter](https://flutter.io/)
5. [Qt Widgets](https://doc.qt.io/qt-5/qtquicklayouts-overview.html) - [examples](https://doc.qt.io/qt-5/qtexamplesandtutorials.html)
6. [Windows Template Studio](https://github.com/Microsoft/WindowsTemplateStudio/) & [XAML Toolkit](https://github.com/Microsoft/Rapid-XAML-Toolkit)
7. [Xamarin](https://docs.microsoft.com/en-us/xamarin)
8. [ClojureScript's Om](https://github.com/omcljs/om)
9. [Vue Native](https://vue-native.io/declarative.html)
