# Ideal DSL Example

## Structure, Style, Layout


### Structure

The structure is a tree, with a root node.

Every node is the same as the root. Each node in the tree (could) have
the following properties:

+ type (defaults to Node/empty)
  + Node (empty), Scroll, Collection, Text, Control, Image, etc
  + each type might have specific properties (not described yet)
    + for example: Text requires content [String] or Image requires an image name [String] OR url [String]
+ style (optional)
  + see below
+ layout (required)
  + see below
+ children (optional)
  + ordered collection of nodes
  + does the node have children, or does the layout?


### Style

Style is a collection of properties. In a single style datum, all
possible properties are optional (you don't have to define every style
property on every style object).

Style properties might be:

+ foreground color (fgColor)
+ background color (bgColor)
+ font name (fontName)
+ font size (fontSize)
+ border color (borderColor)
+ border width (borderWidth)
+ corner radius (cornerRadius)
+ etc


Styles are meant to be inherited from parent nodes.


### Layout

I like the AsyncDisplayKit/Texture layout system. The options it
offers (and their properties) are:

+ stack
  + direction
  + spacing
  + justifyContent (on the stack axis)
  + alignItems (on the cross axis)
+ inset
  + (top, right, bottom, left)
+ overlay
+ background
+ center
  + axis (X, Y, both)
  + sizing fit to smallest X, Y, both, auto
+ ratio
  + relative w:h
+ relative
  + horizontal position
  + vertical position
  + sizing
+ absolute
  + [layout position]
+ wrapper  


Additionally, every layout can have:

+ size
  + either in pixels/points or fraction (relative to parent)


Layouts are also composable. In other words, they're substitute-able
with the nodes in the structure tree.




## Example

```
base = Style(fgColor: blue
             bgColor: white
             fontName: "system"
             fontSize: 18
             )
title = Style(fontName: "title font"
              fontSize: 32)
heading = Style(fontName: "heading font"
                fontSize: 24)              


Scroll( style: base
        layout: Stack(vertical, 20.0, .start, .center,
                      [Text("How does Pink Noise work?" 
                            style: title
                            layout: Overlay([Image("back_button"
                                                   layout: Inset([20,-1,-1,10] 
                                                                 size: Points(w: 50
                                                                              h: 50)))
                                            ])
                           )
                       Text("Pink Noise is a flavor of white noise with deeper tones. It is designed..."
                            layout: Center(both
                                           size: Fraction(w: 0.8)))
                       Text("For Naps" 
                            style: heading
                            layout: Center(both
                                           size: Fraction(w: 0.8)))
                       Image("AboutTourNaps"
                             layout: Center(both,
                                            size: Fraction(w: 0.8)))
                       Text("You can set a timer for 15m, 1h or 2h. The noise will stop playing when the timer completes. ...",
                            layout: Center(both
                                           size: Fraction(w: 0.8)))
                       Text("All Night"
                            style: heading,
                            layout: Center(both
                                           size: Fraction(w: 0.8)))
                       Image("AboutTourAllNight"
                             layout: Center(both,
                                            size: Fraction(w: 0.8)))
                       Text("Pink Noise can be played until you've finished with it. This is ideal for ..."
                            layout: Center(both,
                                            size: Fraction(w: 0.8)))
                       ]
                     )
      )
```


We can later debate 

+ how variables are referenced 
+ how styles or layouts are inherited
+ how layouts might be composed
+ how styles might be merged

This is all possible syntactic sugar to make the high-level definition
simpler & DRY-er.



### Maps or Vectors of structs

This form might be great as a high-level language but we will need a
lower-level structure to describe the complete definition of the
structure that the native views need. It's going to be repetitive but
easily parse-able.

The next question is should the structure be built of structs with
some optional values or maps with enum keys?


```

```


### Open Questions

+ does a Node have a Layout or does a Layout have a Node(s)?
+ layouts are being duplicated a lot (Center, fraction(0.8))
  + Should we have 
    + default inherited Layout
    + named Layouts that can be referenced (like variables)
    + extension types with default values
+ how should the node/layout type be described?
  + a class/struct with a constructor, or
  + a required property in a map
+ how would a table/collection node be represented?
  + content (like text) would get allocated dynamically
  + would need a variable/placeholder for using the node as a template to later create the actual node/view
  
