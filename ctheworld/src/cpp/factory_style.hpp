//
//  factory_style.hpp
//  about
//
//  Created by Adam Tait on 9/11/18.
//

#ifndef factory_style_hpp
#define factory_style_hpp


#include <experimental/any>
#include <map>
#include <string>

#include "style.hpp"
#include "color.hpp"



namespace ctheworld
{
    namespace factory
    {
        namespace style
        {
            using std::string;
            using std::map;
            using std::pair;
            using std::experimental::any;
            
            using ctheworld_gen::Color;
            using ctheworld_gen::Style;
            
            
            enum Prop
            {
                fg_color_t,
                bg_color_t,
                font_name_t,
                font_size_t,
                border_color_t,
                border_width_t,
                corner_radius_t,
                parent_t
            };
            
            typedef map<Prop, any> StyleMap;
            typedef map<string, StyleMap> StyleSheet;
            
            
            pair<Prop, any> parent(const string key);
            pair<Prop, any> fg_color(const Color color);
            pair<Prop, any> bg_color(const Color color);
            pair<Prop, any> font_name(const string name);
            pair<Prop, any> font_size(const float size);
            pair<Prop, any> border_color(const Color color);
            pair<Prop, any> border_width(const float width);
            pair<Prop, any> corner_radius(const float radius);
            
            Style with(const string name,
                       const StyleSheet from);
        }
    };
};

#endif /* factory_style_hpp */
