//
//  factory_layout.cpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "factory_layout.hpp"

namespace ctheworld
{
  using std::experimental::optional;
  using std::experimental::nullopt;
  using ctheworld_gen::NodeType;
  using ctheworld_gen::LayoutType;
  using ctheworld_gen::DimensionType;
    
    
  namespace factory
  {
    // none
    ctheworld_gen::Layout
    layout::none()
    {
      Dimension d = Dimension(DimensionType::FRACTION, 1.0);
      return layout::none(d, d);
    }
        
    ctheworld_gen::Layout
    layout::none(Dimension width,
                 Dimension height)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::NONE,
                    nullopt, nullopt, nullopt, nullopt, nullopt, nullopt, nullopt);
    }
        
        
        
    // center
    ctheworld_gen::Layout
    layout::center()
    {
      return center(LayoutCenterAxisType::BOTH, LayoutCenterSizingType::AUTO);
    }
        
    ctheworld_gen::Layout
    layout::center(Dimension width,
                   Dimension height)
    {
      return center(width, height,
                    LayoutCenterAxisType::BOTH, LayoutCenterSizingType::AUTO);
    }
        
    ctheworld_gen::Layout
    layout::center(LayoutCenterAxisType axis,
                   LayoutCenterSizingType sizing)
    {
      Dimension d = Dimension(DimensionType::FRACTION, 1.0);
      return Layout(optional<Dimension>{d},
                    optional<Dimension>{d},
                    LayoutType::CENTER,
                    nullopt, nullopt, nullopt, nullopt, nullopt,
                    optional<LayoutCenterAxisType>{axis},
                    optional<LayoutCenterSizingType>{sizing});
    }
        
    ctheworld_gen::Layout
    layout::center(Dimension width,
                   Dimension height,
                   LayoutCenterAxisType axis,
                   LayoutCenterSizingType sizing)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::CENTER,
                    nullopt, nullopt, nullopt, nullopt, nullopt,
                    optional<LayoutCenterAxisType>{axis},
                    optional<LayoutCenterSizingType>{sizing});
    }
        
        
    // inset
    Layout
    layout::inset(ctheworld_gen::Insets insets_)
    {
      Dimension d = Dimension(DimensionType::FRACTION, 1.0);
      return Layout(optional<Dimension>{d},
                    optional<Dimension>{d},
                    LayoutType::INSET,
                    nullopt, nullopt, nullopt, nullopt,
                    insets_,
                    nullopt, nullopt);
    }
        
    Layout
    layout::inset(Dimension width,
                  Dimension height,
                  ctheworld_gen::Insets insets_)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::INSET,
                    nullopt, nullopt, nullopt, nullopt,
                    insets_,
                    nullopt, nullopt);
    }
        
        
        
    // stack
    Layout
    layout::stack(float spacing)
    {
      Dimension d = Dimension(DimensionType::FRACTION, 1.0);
      return Layout(optional<Dimension>{d},
                    optional<Dimension>{d},
                    LayoutType::STACK,
                    optional<bool>{false},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::START},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},
                    nullopt, nullopt, nullopt);
    }
        
    Layout
    layout::stack(bool horizontal,
                  float spacing)
    {
      Dimension d = Dimension(DimensionType::FRACTION, 1.0);
      return Layout(optional<Dimension>{d},
                    optional<Dimension>{d},
                    LayoutType::STACK,
                    optional<bool>{horizontal},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::START},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},
                    nullopt, nullopt, nullopt);
    }
        
        
    Layout
    layout::stack(Dimension width,
                  Dimension height,
                  float spacing)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::STACK,
                    optional<bool>{false},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::START},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},
                    nullopt, nullopt, nullopt);
    }
        
    Layout
    layout::stack(Dimension width,
                  Dimension height,
                  bool horizontal,
                  float spacing)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::STACK,
                    optional<bool>{horizontal},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::START},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},
                    nullopt, nullopt, nullopt);
    }
        
    Layout
    layout::stack(bool horizontal,
                  float spacing,
                  LayoutStackJustifyType justify,
                  LayoutStackAlignType align)
    {
      Dimension d = Dimension(DimensionType::FRACTION, 1.0);
      return Layout(optional<Dimension>{d},
                    optional<Dimension>{d},
                    LayoutType::STACK,
                    optional<bool>{horizontal},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{justify},
                    optional<LayoutStackAlignType>{align},
                    nullopt, nullopt, nullopt);
    }
        
        
    Layout
    layout::stack(Dimension width,
                  Dimension height,
                  float spacing,
                  LayoutStackJustifyType justify,
                  LayoutStackAlignType align)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::STACK,
                    optional<bool>{false},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{justify},
                    optional<LayoutStackAlignType>{align},
                    nullopt, nullopt, nullopt);
    }
        
    Layout
    layout::stack(Dimension width,
                  Dimension height,
                  bool horizontal,
                  float spacing,
                  LayoutStackJustifyType justify,
                  LayoutStackAlignType align)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::STACK,
                    optional<bool>{horizontal},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{justify},
                    optional<LayoutStackAlignType>{align},
                    nullopt, nullopt, nullopt);
    }
        
        
    Layout
    layout::stackCentered(Dimension width,
                          Dimension height,
                          float spacing)
    {
      return stackCentered(width, height, false, spacing);
    }
        
    Layout
    layout::stackCentered(Dimension width,
                          Dimension height,
                          bool horizontal,
                          float spacing)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::STACK,
                    optional<bool>{horizontal},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::CENTER},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},         // could also be LayoutStackAlignType::STRETCH
                    nullopt, nullopt, nullopt);
    }
        
        
    Layout
    layout::stackSpaced(Dimension width,
                        Dimension height,
                        float spacing)
    {
      return stackSpaced(width, height, false, spacing);
    }
        
    Layout
    layout::stackSpaced(Dimension width,
                        Dimension height,
                        bool horizontal,
                        float spacing)
    {
      return Layout(optional<Dimension>{width},
                    optional<Dimension>{height},
                    LayoutType::STACK,
                    optional<bool>{horizontal},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::SPACE_AROUND},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},         // could also be LayoutStackAlignType::STRETCH
                    nullopt, nullopt, nullopt);
    }
    
    Layout
    layout::stackSpaced(float spacing)
    {
      return Layout(optional<Dimension>{nullopt},
                    optional<Dimension>{nullopt},
                    LayoutType::STACK,
                    optional<bool>{false},
                    optional<float>{spacing},
                    optional<LayoutStackJustifyType>{LayoutStackJustifyType::CENTER},
                    optional<LayoutStackAlignType>{LayoutStackAlignType::CENTER},         // could also be LayoutStackAlignType::STRETCH
                    nullopt, nullopt, nullopt);
    }
  };
}
