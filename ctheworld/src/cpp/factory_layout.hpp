//
//  factory_layout.hpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef factory_layout_hpp
#define factory_layout_hpp

#include "node.hpp"
#include "dimension.hpp"


namespace ctheworld
{
  using std::string;
  using ctheworld_gen::Layout;
  using ctheworld_gen::Dimension;
  using ctheworld_gen::Insets;
  using ctheworld_gen::LayoutCenterAxisType;
  using ctheworld_gen::LayoutCenterSizingType;
  using ctheworld_gen::LayoutStackJustifyType;
  using ctheworld_gen::LayoutStackAlignType;
    

    
  namespace factory
  {
    struct layout
    {
      static Layout none();
      static Layout none(Dimension width,
                         Dimension height);
            
      static Layout center();
      static Layout center(Dimension width,
                           Dimension height);
      static Layout center(LayoutCenterAxisType axis_,
                           LayoutCenterSizingType sizing_);
      static Layout center(Dimension width,
                           Dimension height,
                           LayoutCenterAxisType axis_,
                           LayoutCenterSizingType sizing_);
            
            
      static Layout inset(Insets insets_);
      static Layout inset(Dimension width,
                          Dimension height,
                          Insets insets_);
            
            
      static Layout stack(float spacing);
      static Layout stack(bool horizontal,
                          float spacing);
      static Layout stack(Dimension width,
                          Dimension height,
                          float spacing);
      static Layout stack(Dimension width,
                          Dimension height,
                          bool horizontal,
                          float spacing);
      static Layout stack(bool horizontal,
                          float spacing,
                          LayoutStackJustifyType justify,
                          LayoutStackAlignType align);
      static Layout stack(Dimension width,
                          Dimension height,
                          float spacing,
                          LayoutStackJustifyType justify,
                          LayoutStackAlignType align);
      static Layout stack(Dimension width,
                          Dimension height,
                          bool horizontal,
                          float spacing,
                          LayoutStackJustifyType justify,
                          LayoutStackAlignType align);
            
      static Layout stackCentered(Dimension width,
                                  Dimension height,
                                  float spacing);
      static Layout stackCentered(Dimension width,
                                  Dimension height,
                                  bool horizontal,
                                  float spacing);
            
      static Layout stackSpaced(Dimension width,
                                Dimension height,
                                float spacing);
      static Layout stackSpaced(Dimension width,
                                Dimension height,
                                bool horizontal,
                                float spacing);
      static Layout stackSpaced(float spacing);
    };
  };
}

#endif /* factory_layout_hpp */
