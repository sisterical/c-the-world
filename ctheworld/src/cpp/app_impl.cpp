//
//  app_impl.cpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 SIsterical Inc. All rights reserved.
//

#include "app_impl.hpp"
#include "root_node.hpp"


namespace ctheworld_gen
{
  using std::make_shared;
  using ctheworld::RootNode;


  
  shared_ptr<App>
  ctheworld_gen::App::create(const shared_ptr<NavigationController> & navigationController)
  {
    return make_shared<AppImpl>(AppImpl(navigationController));
  }
    
    
  AppImpl::AppImpl(const shared_ptr<NavigationController> & navigation_controller)
    : navigationController(navigation_controller),
      sharedState(make_shared<SharedState>(SharedState({})))
  {
    Node n = RootNode::create(navigationController, sharedState);
    navigationController->next(n);
  }
}
