//
//  factory_node.cpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "factory_node.hpp"

namespace ctheworld
{
  using std::make_shared;
  using std::experimental::nullopt;
  using ctheworld_gen::NodeType;
    
    
  namespace factory
  {
    // basic
    Node
    node::basic(Style style_,
                Layout layout_,
                vector<Node> children_)
    {
      return Node(NodeType::BASIC,
                  nullopt,
                  style_,
                  layout_,
                  make_shared<IntEventGenerator>(),
                  make_shared<StringEventGenerator>(),
                  make_shared<IntEventGenerator>(),
                  children_,
                  make_shared<CollectionDataSource>());
    }
        
    Node
    node::basic(Style style_,
                Layout layout_,
                shared_ptr<IntEventGenerator> touches_,
                vector<Node> children_)
    {
      return Node(NodeType::CONTROL,
                  nullopt,
                  style_,
                  layout_,
                  touches_,
                  make_shared<StringEventGenerator>(),
                  make_shared<IntEventGenerator>(),
                  children_,
                  make_shared<CollectionDataSource>());
    }
        
        
        
        
    // text
    Node
    node::text(string content_,
               Style style_,
               Layout layout_)
    {
      return text(content_, style_, layout_, make_shared<StringEventGenerator>(), {});
    }
        
    Node
    node::text(string content_,
               Style style_,
               Layout layout_,
               vector<Node> children_)
    {
      return text(content_, style_, layout_, make_shared<StringEventGenerator>(), children_);
    }
        
    Node
    node::text(string content_,
               Style style_,
               Layout layout_,
               shared_ptr<StringEventGenerator> text_)
    {
      return text(content_, style_, layout_, text_, {});
    }
        
    Node
    node::text(string content_,
               Style style_,
               Layout layout_,
               shared_ptr<StringEventGenerator> text_,
               vector<Node> children_)
    {
      return Node(NodeType::TEXT,
                  optional<string>{content_},
                  style_,
                  layout_,
                  make_shared<IntEventGenerator>(),
                  text_,
                  make_shared<IntEventGenerator>(),
                  children_,
                  make_shared<CollectionDataSource>());
    }
        
        
    // image
    Node
    node::image(string imageName_,
                Style style_,
                Layout layout_)
    {
      return Node(NodeType::IMAGE,
                  optional<string>{imageName_},
                  style_,
                  layout_,
                  make_shared<IntEventGenerator>(),
                  make_shared<StringEventGenerator>(),
                  make_shared<IntEventGenerator>(),
                  {},
                  make_shared<CollectionDataSource>());
    }
        
        
        
    // scroll
        
    Node
    node::scroll(Style style_,
                 Layout layout_,
                 vector<Node> children_)
    {
      return Node(NodeType::SCROLL,
                  nullopt,
                  style_,
                  layout_,
                  make_shared<IntEventGenerator>(),
                  make_shared<StringEventGenerator>(),
                  make_shared<IntEventGenerator>(),
                  children_,
                  make_shared<CollectionDataSource>());
    }
        
        
    // collection
    Node
    node::collection(Style style_,
                     Layout layout_,
                     shared_ptr<CollectionDataSource> data_)
    {
      return Node(NodeType::COLLECTION,
                  nullopt,
                  style_,
                  layout_,
                  make_shared<IntEventGenerator>(),
                  make_shared<StringEventGenerator>(),
                  make_shared<IntEventGenerator>(),
                  {},
                  data_);
    }
  };
}
