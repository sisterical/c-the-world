//
//  string_event_generator.hpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef string_event_generator_hpp
#define string_event_generator_hpp

#include "../generated-src/cpp/string_event_generator.hpp"
#include <experimental/functional>

namespace ctheworld
{
    using std::function;
    using std::vector;
    using std::string;
    
    
    class StringEventGenerator : public ctheworld_gen::StringEventGenerator
    {
    public:
        StringEventGenerator()  {}
        StringEventGenerator(function<void(const string &)> f) : fs({f})     {}
        
        virtual bool is_active() override;
        virtual void event(const string & datum) override;
        void add_observer(function<void(const string &)> f);
        
    private:
        vector<function<void(const string &)>> fs;
    };
}

#endif /* string_event_generator_hpp */
