//
//  int_event_generator.hpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef int_event_generator_hpp
#define int_event_generator_hpp

#include "../generated-src/cpp/int_event_generator.hpp"
#include <cstdio>
#include <experimental/functional>

namespace ctheworld
{
    using std::function;
    using std::vector;
    
    
    class IntEventGenerator : public ctheworld_gen::IntEventGenerator
    {
    public:
        IntEventGenerator() {}
        IntEventGenerator(function<void(int64_t)> f)  : fs({f})  {}
        
        virtual bool is_active() override;
        virtual void event(int64_t datum) override;
        void add_observer(function<void(int64_t)> f);
        
    private:
        vector<function<void(int64_t)>> fs;
    };
}

#endif /* int_event_generator_hpp */
