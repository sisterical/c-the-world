//
//  collection_data_source.hpp
//  ctheworld
//
//  Created by Adam Tait on 8/14/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef collection_data_source_hpp
#define collection_data_source_hpp

#include "../generated-src/cpp/collection_data_source.hpp"
#include "../generated-src/cpp/node.hpp"
#include <cstdio>
#include <experimental/functional>

namespace ctheworld
{
    using std::function;
    using std::vector;
    using ctheworld_gen::Node;
    
    
    class CollectionDataSource : public ctheworld_gen::CollectionDataSource
    {
    public:
        CollectionDataSource() : s(0), f({}) {}
        CollectionDataSource(int64_t size,
                             function<Node(int64_t)> fn)
        : s(size), f(fn)
        {}
        
        virtual int64_t count() override    { return s; }
        
        virtual Node node(int64_t index) override   { return f(index); }
        
        
    private:
        int64_t s;
        function<Node(int64_t)> f;
    };
}

#endif /* collection_data_source_hpp */
