//
//  shared_state.hpp
//  ctheworld
//
//  Created by Adam Tait on 9/17/18.
//  Copyright © 2018 SIsterical Inc. All rights reserved.
//

#pragma once

#include <memory>
#include <map>
#include <string>
#include <experimental/any>


namespace ctheworld
{
  using std::map;
  using std::string;
  using std::experimental::any;
    

  typedef map<string, any> SharedState;
}
