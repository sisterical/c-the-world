//
//  log.hpp
//  ctheworld
//
//  Created by Adam Tait on 9/25/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//


// taken from https://stackoverflow.com/questions/24325463/how-to-get-console-output-of-log-lines-printf-cout-etc-of-c-library-use

#ifdef ANDROID  // LOGS FOR ANDROID NDK
            
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG,__VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , LOG_TAG,__VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , LOG_TAG,__VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , LOG_TAG,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , LOG_TAG,__VA_ARGS__)
#define LOGSIMPLE(...)

#else   // LOGS NOT ANDROID
            
#include <stdio.h>
#define LOGV(...) printf("  "); printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#define LOGD(...) printf("  "); printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#define LOGI(...) printf("  "); printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#define LOGW(...) printf("  * Warning: "); printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#define LOGE(...) printf("  *** Error:  "); printf(__VA_ARGS__); printf("\t -  <%s> \n", LOG_TAG);
#define LOGSIMPLE(...) printf(" "); printf(__VA_ARGS__);

#endif // ANDROID
