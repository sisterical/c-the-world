//
//  int_event_generator.cpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "int_event_generator.hpp"

namespace ctheworld
{
    bool IntEventGenerator::is_active()    { return fs.size() > 0; }
    
    void IntEventGenerator::event(int64_t datum)
    {
        for (function<void(int64_t)> f : fs)   { f(datum); }
    }
    
    void IntEventGenerator::add_observer(function<void(int64_t)> f)
    {
        fs.push_back(f);
    }
}
