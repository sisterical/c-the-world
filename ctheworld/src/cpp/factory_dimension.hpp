//
//  factory_dimension.hpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef factory_dimension_hpp
#define factory_dimension_hpp

#include "dimension.hpp"


namespace ctheworld
{
  using ctheworld_gen::Dimension;
    
    
  namespace factory
  {
    struct dim
    {
      static Dimension fraction(float p);
      static Dimension full();
      static Dimension points(float px);
      static Dimension zero();
    };
  };
}

#endif /* factory_dimension_hpp */
