//
//  factory_generator.hpp
//  ctheworld
//
//  Created by Adam Tait on 6/14/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef factory_generator_hpp
#define factory_generator_hpp

#include "bool_event_generator.hpp"
#include "int_event_generator.hpp"
#include "string_event_generator.hpp"


namespace ctheworld
{
  using std::string;
  using std::shared_ptr;
  using ctheworld::BoolEventGenerator;
  using ctheworld::IntEventGenerator;
  using ctheworld::StringEventGenerator;

    
  namespace factory
  {
    struct gen
    {
      static shared_ptr<BoolEventGenerator> b(function<void(bool)> f);
      static shared_ptr<IntEventGenerator> i(function<void(int64_t)> f);
      static shared_ptr<StringEventGenerator> s(function<void(const string &)> f);
    };
  }

}

#endif /* factory_generator_hpp */
