//
//  factory_dimension.cpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "factory_dimension.hpp"

namespace ctheworld
{
  using ctheworld_gen::DimensionType;
    
    
    
    
  namespace factory
  {
    ctheworld_gen::Dimension
    dim::fraction(float p)
    {
      return Dimension(DimensionType::FRACTION, p);
    }

    ctheworld_gen::Dimension
    dim::full()
    {
      return dim::fraction(1.0);
    }
        
    ctheworld_gen::Dimension
    dim::points(float px)
    {
      return Dimension(DimensionType::POINTS, px);
    }
        
    ctheworld_gen::Dimension
    dim::zero()
    {
      return dim::points(0.0);
    }
  };
}
