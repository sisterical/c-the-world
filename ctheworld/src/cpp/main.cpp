//
//  main.cpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 SIsterical Inc. All rights reserved.
//

#include "root_node.hpp"
#include "navigation_controller_mock.hpp"
#include "shared_state.hpp"


using std::shared_ptr;
using std::make_shared;
using ctheworld_gen::NavigationController;
using ctheworld::SharedState;
using sisterical_test::NavigationControllerMock;


int main(int argc, const char * argv[])
{
  (void)argv[0];
  (void)argc;

  // a small test
  shared_ptr<NavigationController> nav = make_shared<NavigationControllerMock>(NavigationControllerMock());
  shared_ptr<SharedState> state = make_shared<SharedState>(SharedState({}));
  ctheworld_gen::Node n = ctheworld::RootNode::create(nav, state);
  return n.children.size() > 0;
}
