//
//  factory_node.hpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef factory_node_hpp
#define factory_node_hpp

#include "node.hpp"
#include "bool_event_generator.hpp"
#include "int_event_generator.hpp"
#include "string_event_generator.hpp"
#include "collection_data_source.hpp"


namespace ctheworld
{
  using std::string;
  using std::vector;
  using std::shared_ptr;
  using std::experimental::optional;
  using ctheworld_gen::Node;
  using ctheworld_gen::Style;
  using ctheworld_gen::Layout;
  using ctheworld::BoolEventGenerator;
  using ctheworld::IntEventGenerator;
  using ctheworld::StringEventGenerator;
  using ctheworld::CollectionDataSource;
    
    

    
  namespace factory
  {
    struct node
    {
      // basic
      static Node basic(Style style_,
                        Layout layout_,
                        vector<Node> children_);
            
      static Node basic(Style style_,
                        Layout layout_,
                        shared_ptr<IntEventGenerator> touches_,
                        vector<Node> children_);
            
            
      // text
      static Node text(string content_,
                       Style style_,
                       Layout layout_);
            
      static Node text(string content_,
                       Style style_,
                       Layout layout_,
                       vector<Node> children_);
            
      static Node text(string content_,
                       Style style_,
                       Layout layout_,
                       shared_ptr<StringEventGenerator> text_);
            
      static Node text(string content_,
                       Style style_,
                       Layout layout_,
                       shared_ptr<StringEventGenerator> text_,
                       vector<Node> children_);
            
      // image
      static Node image(string imageName_,
                        Style style_,
                        Layout layout_);
            
      // scroll
      static Node scroll(Style style_,
                         Layout layout_,
                         vector<Node> children_);
            
            
      // collection
      static Node collection(Style style_,
                             Layout layout_,
                             shared_ptr<CollectionDataSource> children);
    };
  };
}

#endif /* factory_node_hpp */
