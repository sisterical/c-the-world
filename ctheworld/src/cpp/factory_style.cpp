//
//  factory_style.cpp
//  about
//
//  Created by Adam Tait on 9/11/18.
//

#include <vector>
#include "factory_style.hpp"


namespace ctheworld
{
    namespace factory
    {
        namespace style
        {
            using std::make_pair;
            using std::vector;
            using std::experimental::any_cast;
            

            pair<Prop, any>
            parent(const string key)
            {
                return make_pair(parent_t, any(key));
            }
            
            pair<Prop, any>
            fg_color(const Color color)
            {
                return make_pair(fg_color_t, any(color));
            }
            
            pair<Prop, any>
            bg_color(const Color color)
            {
                return make_pair(bg_color_t, any(color));
            }
            
            pair<Prop, any>
            font_name(const string name)
            {
                return make_pair(font_name_t, any(name));
            }
            
            pair<Prop, any>
            font_size(const float size)
            {
                return make_pair(font_size_t, any(size));
            }
            
            pair<Prop, any>
            border_color(const Color color)
            {
                return make_pair(border_color_t, any(color));
            }
            
            pair<Prop, any>
            border_width(const float width)
            {
                return make_pair(border_width_t, any(width));
            }
            
            pair<Prop, any>
            corner_radius(const float radius)
            {
                return make_pair(corner_radius_t, any(radius));
            }
            
            
            
            
            const map<Prop, string> propToStringMap = {
                {fg_color_t, "fg_color"},
                {bg_color_t, "bg_color"},
                {font_name_t, "font_name"},
                {font_size_t, "font_size"},
                {border_color_t, "border_color"},
                {border_width_t, "border_width"},
                {corner_radius_t, "corner_radius"} };
            
            
            const StyleMap
            merge(const StyleMap s,
                  const StyleMap st)
            {
                StyleMap r = {};
                for(auto const &p : propToStringMap)
                {
                    if (st.find(p.first) != st.end() )
                    {
                        any v = st.find(p.first)->second;
                        if ( ! v.empty() )
                        {   // take value from st
                            r[p.first] = v;
                            continue;
                        }
                    }
                    
                    // take value from s
                    r[p.first] = s.find(p.first)->second;
                }
                return r;
            }
            
            template <typename T>
            T cast(any v)
            {
                return *any_cast<T>(&v);
            }
            
            Style
            intoStyle( StyleMap s )
            // assumes style map has been validated
            {
                return Style(cast<Color>(s.at(fg_color_t)),
                             cast<Color>(s.at(bg_color_t)),
                             cast<string>(s.at(font_name_t)),
                             cast<float>(s.at(font_size_t)),
                             cast<Color>(s.at(border_color_t)),
                             cast<float>(s.at(border_width_t)),
                             cast<float>(s.at(corner_radius_t)));
            }
            
            bool
            valid(const StyleMap s)
            {
                bool r = true;
                for(auto const &p : propToStringMap)
                {
                    r = r && ! s.find(p.first)->second.empty();
                    if (!r) {
                        printf("--- WARNING ---\nfactory_style.cpp: %s style property is missing\n", p.second.c_str());
                    }
                }
                return r;
            }
            
            
            
            
            Style
            with(const string name,
                 const StyleSheet from)
            {
                StyleMap s = from.at(name);
                
                // handle parent relationships
                if ( s.find(parent_t) != s.end() )
                {
                    vector<string> parents = { name };
                    StyleMap tmp = s;
                    while ( tmp.find(parent_t) != tmp.end() )
                    {
                        string next_parent = cast<string>(tmp.at(parent_t));
                        parents.push_back( next_parent );
                        tmp = from.at( next_parent );
                    }
                    
                    s = from.at(cast<string>(parents.back()));
                    for (vector<string>::reverse_iterator it = parents.rbegin();
                         it != parents.rend();
                         ++it)
                    {
                        if (it == parents.rbegin() )
                        {   // already loaded in the first/lowest/oldest style
                            continue;
                        }
                        
                        s = merge(s, from.at(*it));
                    }
                }
                
                return intoStyle(s);
            }

        }
    }
}

