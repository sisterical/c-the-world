//
//  bool_event_generator.hpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef bool_event_generator_hpp
#define bool_event_generator_hpp

#include "../generated-src/cpp/bool_event_generator.hpp"
#include <cstdio>
#include <experimental/functional>


namespace ctheworld
{
    using std::function;
    using std::vector;
    
    
    class BoolEventGenerator : public ctheworld_gen::BoolEventGenerator
    {
    public:
        BoolEventGenerator() {}
        BoolEventGenerator(function<void(bool)> f) : fs({f})  {}
        
        virtual bool is_active() override;
        virtual void event(bool datum) override;
        void add_observer(function<void(bool)> f);
        
    private:
        vector<function<void(bool)>> fs;
    };
}

#endif /* bool_event_generator_impl_hpp */
