//
//  app_impl.hpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 SIsterical Inc. All rights reserved.
//

#ifndef app_impl_hpp
#define app_impl_hpp

#include "app.hpp"
#include "navigation_controller.hpp"
#include "shared_state.hpp"



namespace ctheworld_gen
{
  using std::shared_ptr;
  using ctheworld::SharedState;    

  
  /**
   * App Implementation
   *
   */
  class AppImpl : public App
  {
  public:
        
    // Constructor
    AppImpl(const shared_ptr<NavigationController> & navigationController);
        
    // public interface
    static shared_ptr<App> create(const shared_ptr<NavigationController> & navigation_controller);
        
  private:
    shared_ptr<NavigationController> navigationController;
    shared_ptr<SharedState> sharedState;
        
  };
}

#endif /* app_impl_hpp */
