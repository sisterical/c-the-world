//
//  factory_generator.cpp
//  ctheworld
//
//  Created by Adam Tait on 9/10/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "factory_generator.hpp"

namespace ctheworld
{
  using std::make_shared;
    
    
  namespace factory
  {
    // generator
        
    shared_ptr<BoolEventGenerator>
    gen::b(function<void(bool)> f)
    {
      return make_shared<BoolEventGenerator>(BoolEventGenerator(f));
    }
        
    shared_ptr<IntEventGenerator>
    gen::i(function<void(int64_t)> f)
    {
      return make_shared<IntEventGenerator>(IntEventGenerator(f));
    }
        
    shared_ptr<StringEventGenerator>
    gen::s(function<void(const string &)> f)
    {
      return make_shared<StringEventGenerator>(StringEventGenerator(f));
    }
  };
}
