//
//  bool_event_generator.cpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "bool_event_generator.hpp"


namespace ctheworld
{
    bool BoolEventGenerator::is_active()    { return fs.size() > 0; }
    
    void BoolEventGenerator::event(bool datum)
    {
        for (function<void(bool)> f : fs)   { f(datum); }
    }
    
    void BoolEventGenerator::add_observer(function<void(bool)> f)
    {
        fs.push_back(f);
    }
}
