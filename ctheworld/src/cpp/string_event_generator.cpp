//
//  string_event_generator.cpp
//  ctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "string_event_generator.hpp"


namespace ctheworld
{
    bool StringEventGenerator::is_active()    { return fs.size() > 0; }
    
    void StringEventGenerator::event(const string & datum)
    {
        for (function<void(const string &)> f : fs)   { f(datum); }
    }
    
    void StringEventGenerator::add_observer(function<void(const string &)> f)
    {
        fs.push_back(f);
    }
}
