//
//  CTWOEditableTextNode.m
//  libctheworld
//
//  Created by Adam Tait on 9/7/18.
//

#import "CTWOEditableTextNode.h"
#import "CtwStringEventGenerator.h"



@interface CTWOEditableTextNode () <ASEditableTextNodeDelegate>
{
    CtwNode *_data;
}

@end



@implementation CTWOEditableTextNode

- (instancetype _Nonnull) initWithCtwNode:(CtwNode *_Nonnull) ctwNode
{
    if ( !(self = [super init]) )       return nil;
    
    _data = ctwNode;
    self.delegate = self;
    
    // set initial attributedText
    if (_data.content != nil)
    {
        self.attributedText = [[NSAttributedString alloc] initWithString:_data.content
                                                            attributes:@{}];
    }
    
    return self;
}


#pragma mark ASEditableTextNodeDelegate

- (void) editableTextNodeDidUpdateText:(ASEditableTextNode *) editableTextNode
{
    (void)editableTextNode;
    NSString *s = self.attributedText.string != nil ? self.attributedText.string : @"";     // DJINNI marshalling will crash if nil
    [_data.text event:s];
}


@end
