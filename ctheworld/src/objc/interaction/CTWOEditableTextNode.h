//
//  CTWOEditableTextNode.h
//  libctheworld
//
//  Created by Adam Tait on 9/7/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwNode.h"


@interface CTWOEditableTextNode : ASEditableTextNode

- (instancetype _Nonnull) initWithCtwNode:(CtwNode *_Nonnull) ctwNode;

@end
