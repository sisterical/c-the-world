//
//  ASControlNode+Touches.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import "ASControlNode+Touches.h"


@implementation ASControlNode (Touches)


#pragma mark touches property

- (MutableProperty<NSNumber*>*) touches
{   // retrieve _touches, as associated object
    return objc_getAssociatedObject(self, @selector(_touches));
}


#pragma mark enable

- (void) enableTouches
{
    // setup _touches, as associated object
    MutableProperty<NSNumber*> *t = [[MutableProperty<NSNumber*> alloc] initWith:[NSNumber numberWithInt:0] ];
    objc_setAssociatedObject(self, @selector(_touches), t, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // setup ASControlNode (self)
    [self setEnabled:true];
    [self setUserInteractionEnabled:true];
    [self addTarget:self
             action:@selector(touched)
   forControlEvents:ASControlNodeEventTouchUpInside];
}


- (void) touched
{
    NSInteger v = [[self.touches get] integerValue];
    [self.touches set:[NSNumber numberWithInteger:(v+1)]];
}

@end
