//
//  ASControlNode+Touches.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "MutableProperty.h"


@interface ASControlNode (Touches)

- (void) enableTouches;
- (MutableProperty<NSNumber*>*) touches;

@end
