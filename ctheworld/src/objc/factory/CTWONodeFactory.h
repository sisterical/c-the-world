//
//  CTWONodeFactory.h
//  libctheworld
//
//  Created by Adam Tait on 8/14/18.
//

#import <Foundation/Foundation.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwNode.h"



@interface CTWONodeFactory : NSObject

+ (ASViewController *) viewControllerFrom:(CtwNode *) data;
+ (ASDisplayNode *) nodeFrom:(CtwNode *) data;
+ (ASCellNode *) cellNodeFrom:(CtwNode *) data;

@end
