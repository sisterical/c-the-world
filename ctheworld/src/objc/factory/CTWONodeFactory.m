//
//  CTWONodeFactory.m
//  libctheworld
//
//  Created by Adam Tait on 8/14/18.
//

#import "CTWONodeFactory.h"
#import "CtwIntEventGenerator.h"
#import "CtwStringEventGenerator.h"
#import "CTWOCollectionNode.h"
#import "ASDisplayNode+Style.h"
#import "ASDisplayNode+Layout.h"
#import "ASTextNode+Layout.h"
#import "ASTextNode+Style.h"
#import "ASEditableTextNode+Layout.h"
#import "ASEditableTextNode+Style.h"
#import "ASControlNode+Touches.h"
#import "ASScrollNode+Layout.h"
#import "CTWOEditableTextNode.h"





@implementation CTWONodeFactory

+ (ASViewController *) viewControllerFrom:(CtwNode *) data
{
    ASDisplayNode *node = [CTWONodeFactory nodeFrom:data];
    ASViewController *vc = [[ASViewController alloc] initWithNode:node];
    return vc;
}


+ (ASDisplayNode *) nodeFrom:(CtwNode *) data
{
    ASDisplayNode *n;
    switch (data.type)
    {
        case CtwNodeTypeBasic: {
            if (data.touches != nil
                && [data.touches isActive])
            { n = [[ASControlNode alloc] init]; }
            else { n = [[ASDisplayNode alloc] init]; }
            break;
        }
        case CtwNodeTypeControl: {
            n = [[ASControlNode alloc] init];
            break;
        }
        case CtwNodeTypeScroll: {
            n = [[ASScrollNode alloc] init];
            break;
        }
        case CtwNodeTypeCollection: {
            n = [[CTWOCollectionNode alloc] initWithCtwNode:data];
            break;
        }
        case CtwNodeTypeText: {
            
            if (data.text != nil
                && [data.text isActive])
            {   // editable text node
                n = [[CTWOEditableTextNode alloc] initWithCtwNode:data];
                break;
            }
            
            // standard text node
            n = [[ASTextNode alloc] init];
            if (data.content != nil)
            {
                ASTextNode *tn = (ASTextNode *)n;
                tn.attributedText = [[NSAttributedString alloc] initWithString:data.content attributes:@{}];
            }
            break;
        }
        case CtwNodeTypeImage: {
            
            NSAssert(data.content != nil,
                     nodeFactoryErrorMsg(@"NodeType::IMAGE requires Node.content to be set. Node.content should be an image name.", nil));
            
            UIImage *image = [UIImage imageNamed:data.content];
            NSAssert(image != nil,
                     nodeFactoryErrorMsg([NSString stringWithFormat:@"Image named [%@] cannot be found", data.content], nil));
            
            n = [[ASImageNode alloc] init];
            ASImageNode *imn = (ASImageNode *) n;
            imn.image = image;
            break;
        }
        default: {
            n = [[ASDisplayNode alloc] init];
            break;
        }
    }
    
    setPropertiesOf(n, data);
    return n;
}


+ (ASCellNode *) cellNodeFrom:(CtwNode *) data
{   // for ASCollectionNode & ASTableNode
    NSAssert( data.type == CtwNodeTypeBasic,
             nodeFactoryErrorMsg(@"Children of NODE_TYPE::COLLECTION must be NODE_TYPE::BASIC. The root node must be NODE_TYPE::BASIC because it gets replaced with a CellNode.", data));
    
    ASCellNode *n = [[ASCellNode alloc] init];
    setPropertiesOf(n, data);
    return n;
}

NSString* nodeFactoryErrorMsg(NSString *body, id o)
{
    NSString *objectContentStr = @"";
    if (o != nil)
    {
        objectContentStr = [NSString stringWithFormat:@"\n------------------------------------------------\n%@", o];
    }
    return [NSString stringWithFormat:@"\n\n--- Validation Error ---\n%@\n%@\n------------------------------------------------\n\n", body, objectContentStr];
}





#pragma mark Private

ASDisplayNode* setPropertiesOf(ASDisplayNode *node, CtwNode *data)
{
    [node style:data.style];
    [node layout:data];
    handleTouches(node, data);
    return node;
}

void handleTouches(ASDisplayNode *node, CtwNode *data)
{
    if ([node class] == [ASControlNode class]
        && [data.touches isActive])
    {
        CtwIntEventGenerator *gen = data.touches;
        ASControlNode *n = (ASControlNode *)node;
        
        [n enableTouches];
        [n.touches addObserver:^(NSNumber *numTouches) {
            int64_t v = (int64_t) [numTouches integerValue];
            [gen event: v];
        }];
    }
}


@end
