//
//  ASTextNode+Layout.m
//  libctheworld
//
//  Created by Adam Tait on 8/19/18.
//

#import "ASTextNode+Layout.h"
#import "ASDisplayNode+Layout.h"



@implementation ASTextNode (CtwLayout)

- (void) layout:(CtwNode *) data
{
    // validate
    NSAssert(   (data.layout.type != CtwLayoutTypeNone)
             || (data.layout.type != CtwLayoutTypeCenter),
             textLayoutErrorMsg(@"NodeType::TEXT must have LayoutType::CENTER or LayoutType::NONE.", data));
    
    // if the validations passed, then ...
    [ASDisplayNode applySizeTo:self with:data.layout];
    
    
    // use NSAttributedString.paragraphStyle to layout text    
    NSMutableAttributedString *ms = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    [ms addAttribute:NSParagraphStyleAttributeName
               value:paragraphStyle(data.layout)
               range:NSMakeRange(0, [ms.string length])];
    self.attributedText = ms;
}



NSParagraphStyle* paragraphStyle(CtwLayout *layout)
{
    NSMutableParagraphStyle *ps = [[NSMutableParagraphStyle alloc] init];
    if (layout.type == CtwLayoutTypeCenter)
    {
        ps.alignment = NSTextAlignmentCenter;
    }
    else if (layout.alignItems != nil)
    {
        CtwLayoutStackAlignType t = layout.alignItems;
        switch (t) {
            case CtwLayoutStackAlignTypeStart:          { ps.alignment = NSTextAlignmentLeft; }
            case CtwLayoutStackAlignTypeEnd:            { ps.alignment = NSTextAlignmentRight; }
            case CtwLayoutStackAlignTypeCenter:         { ps.alignment = NSTextAlignmentCenter; }
            case CtwLayoutStackAlignTypeStretch:        { ps.alignment = NSTextAlignmentNatural; }
            case CtwLayoutStackAlignTypeBaselineFirst:  { ps.alignment = NSTextAlignmentJustified; }
            case CtwLayoutStackAlignTypeBaselineLast:
            case CtwLayoutStackAlignTypeNone:
            default:
                break;
        }
    }
    if (layout.spacing != nil) {
        ps.lineSpacing = (CGFloat) [layout.spacing floatValue];
    }
    return ps;
}




#pragma mark Layout Validation

NSString* textLayoutErrorMsg(NSString *body, id o)
{
    return [NSString stringWithFormat:@"\n\n--- Validation Error ---\n%@\n\n------------------------------------------------\n%@\n------------------------------------------------\n\n", body, o];
}



@end
