//
//  ASEditableTextNode+Layout.h
//  libctheworld
//
//  Created by Adam Tait on 9/6/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwNode.h"
#import "CtwLayout.h"


// NOTE: ASEditableTextNode required a separate category from
//  ASTextNode because their only common ancestor is ASDisplayNode

@interface ASEditableTextNode (Layout)

- (void) layout:(CtwNode *) data;

@end
