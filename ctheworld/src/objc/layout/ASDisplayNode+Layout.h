//
//  ASDisplayNode+Layout.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwLayout.h"
#import "CtwNode.h"


@interface ASDisplayNode (CtwLayout)

- (void) layout:(CtwNode *) data;

// helpers
+ (void) applySizeTo:(ASDisplayNode *) node with:(CtwLayout *) layout;
+ (void) layoutChildrenOf:(ASDisplayNode *) node with:(CtwNode *) data;
+ (BOOL) validateLayoutFor:(CtwNode *) data;

@end
