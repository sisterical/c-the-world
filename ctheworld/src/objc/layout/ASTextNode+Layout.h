//
//  ASTextNode+Layout.h
//  libctheworld
//
//  Created by Adam Tait on 8/19/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwNode.h"
#import "CtwLayout.h"


@interface ASTextNode (CtwLayout)

- (void) layout:(CtwNode *) data;

@end
