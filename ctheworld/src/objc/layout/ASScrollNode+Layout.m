//
//  ASScrollNode+Layout.m
//  libctheworld
//
//  Created by Adam Tait on 8/20/18.
//

#import "ASScrollNode+Layout.h"
#import "ASDisplayNode+Layout.h"

@implementation ASScrollNode (CtwLayout)

- (void) layout:(CtwNode *) data
{
    [ASDisplayNode validateLayoutFor:data];
    
    // if validations passed, then ...
    
    self.automaticallyManagesContentSize = true;
    
    // TODO scrollable directions should allow scrolling in both Horizontal & Vertical at the same time
    // --- I think you would define like: ASScrollDirectionHorizontalDirections & ASScrollDirectionVerticalDirections  b/c each enum value is a bit shifted value
    self.scrollableDirections = [data.layout.horizontal boolValue] ? ASScrollDirectionHorizontalDirections : ASScrollDirectionVerticalDirections;
    
    // applying self.style.width OR self.style.height to ASScrollNode seems to break scrolling by changing the internal calculation for content size.
    // to circumvent this issue, you may want to wrap the ScrollNode in a basic node with size constraints.
//    [ASDisplayNode applySizeTo:self with:data.layout];
    [ASDisplayNode layoutChildrenOf:self with:data];
    
    
    
}

@end
