//
//  ASDisplayNode+Layout.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <UIKit/UIKit.h>
#import "ASDisplayNode+Layout.h"
#import "CTWONodeFactory.h"




@implementation ASDisplayNode (CtwLayout)


#pragma mark private helpers

NSString* layoutTypeName(CtwLayoutType t)
{
    switch (t)
    {
        case CtwLayoutTypeNone:         { return @"NONE"; }
        case CtwLayoutTypeStack:        { return @"STACK"; }
        case CtwLayoutTypeInset:        { return @"INSET"; }
        case CtwLayoutTypeOverlay:      { return @"OVERLAY"; }
        case CtwLayoutTypeBackground:   { return @"BACKGROUND"; }
        case CtwLayoutTypeCenter:       { return @"CENTER"; }
    }
}

ASCenterLayoutSpecCenteringOptions centerCenteringOptions (CtwLayout *layout)
{
    CtwLayoutCenterAxisType t = (CtwLayoutCenterAxisType) layout.axis.integerValue;
    switch (t)
    {
        case CtwLayoutCenterAxisTypeX:  { return ASCenterLayoutSpecCenteringX; }
        case CtwLayoutCenterAxisTypeY:  { return ASCenterLayoutSpecCenteringY; }
        case CtwLayoutCenterAxisTypeBoth: { return ASCenterLayoutSpecCenteringXY; }
        default:
            break;
    }
    return ASCenterLayoutSpecCenteringNone;
}

ASCenterLayoutSpecSizingOptions centerSizingOptions (CtwLayout *layout)
{
    CtwLayoutCenterSizingType t = (CtwLayoutCenterSizingType) layout.sizing.integerValue;
    switch (t)
    {
        case CtwLayoutCenterSizingTypeX:    { return ASCenterLayoutSpecSizingOptionMinimumX; }
        case CtwLayoutCenterSizingTypeY:    { return ASCenterLayoutSpecSizingOptionMinimumY; }
        case CtwLayoutCenterSizingTypeBoth: { return ASCenterLayoutSpecSizingOptionMinimumXY; }
        case CtwLayoutCenterSizingTypeAuto:
        default:
            break;
    }
    return ASCenterLayoutSpecSizingOptionDefault;
}


ASStackLayoutJustifyContent stackJustifyContentFrom (CtwLayout *layout)
{
    CtwLayoutStackJustifyType t = (CtwLayoutStackJustifyType) layout.justifyContent.integerValue;
    if (t == CtwLayoutStackJustifyTypeStart)                { return ASStackLayoutJustifyContentStart; }
    else if (t == CtwLayoutStackJustifyTypeCenter)          { return ASStackLayoutJustifyContentCenter; }
    else if (t == CtwLayoutStackJustifyTypeEnd)             { return ASStackLayoutJustifyContentEnd; }
    else if (t == CtwLayoutStackJustifyTypeSpaceBetween)    { return ASStackLayoutJustifyContentSpaceBetween; }
    else if (t == CtwLayoutStackJustifyTypeSpaceAround)     { return ASStackLayoutJustifyContentSpaceAround; }
    
    // default
    return ASStackLayoutJustifyContentCenter;
}

ASStackLayoutAlignItems stackAlignItemsFrom (CtwLayout * layout)
{
    CtwLayoutStackAlignType t = (CtwLayoutStackAlignType) layout.alignItems.integerValue;
    if (t == CtwLayoutStackAlignTypeStart)              { return ASStackLayoutAlignItemsStart; }
    else if (t == CtwLayoutStackAlignTypeEnd)           { return ASStackLayoutAlignItemsEnd; }
    else if (t == CtwLayoutStackAlignTypeCenter)        { return ASStackLayoutAlignItemsCenter; }
    else if (t == CtwLayoutStackAlignTypeStretch)       { return ASStackLayoutAlignItemsStretch; }
    else if (t == CtwLayoutStackAlignTypeBaselineFirst) { return ASStackLayoutAlignItemsBaselineFirst; }
    else if (t == CtwLayoutStackAlignTypeBaselineLast)  { return ASStackLayoutAlignItemsBaselineLast; }
    
    // default
    return ASStackLayoutAlignItemsCenter;
}

ASDimensionUnit dimUnit(CtwDimension *d)
{
    return d.type == CtwDimensionTypeFraction ? ASDimensionUnitFraction : ASDimensionUnitPoints;
}


UIEdgeInsets insets(CtwLayout *layout)
{
    if (layout.insets == nil)       { return UIEdgeInsetsZero; }
    
    CtwInsets *i = layout.insets;
    return UIEdgeInsetsMake(i.top, i.left, i.bottom, i.right);
}

NSArray<ASDisplayNode*>* children(NSArray<CtwNode*>* cs)
{
    NSMutableArray *mCs = [[NSMutableArray alloc] initWithCapacity:cs.count];
    for (CtwNode *n in cs) {
        [mCs addObject:[CTWONodeFactory nodeFrom:n]];
    }
    return [NSArray arrayWithArray:mCs];
}





#pragma mark public implementation

- (void) layout:(CtwNode *) data
{
    [ASDisplayNode validateLayoutFor:data];  // throws NSAssert
    
    
    // if validations passed, then ...
    
    [ASDisplayNode applySizeTo:self with:data.layout];
    [ASDisplayNode layoutChildrenOf:self with:data];
}


+ (void) applySizeTo:(ASDisplayNode *) node
                with:(CtwLayout *) layout
{
    if (layout.width != nil)
    {
        CtwDimension *dw = layout.width;
        node.style.width = ASDimensionMake(dimUnit(dw), dw.value);
    }
    
    if (layout.height != nil)
    {
        CtwDimension *dh = layout.height;
        node.style.height = ASDimensionMake(dimUnit(dh), dh.value);
    }
}


+ (void) layoutChildrenOf:(ASDisplayNode *) node
                     with:(CtwNode *) data
{
    CtwLayout *layout = data.layout;
    NSArray<CtwNode*> *cs = data.children;
    CtwNode *onlyChild = [cs firstObject];
    
    node.automaticallyManagesSubnodes = true;
    node.layoutSpecBlock = ^ASLayoutSpec * _Nonnull(__kindof ASDisplayNode * _Nonnull node, ASSizeRange constrainedSize)
    {
        (void)constrainedSize;
        switch (layout.type)
        {
            case CtwLayoutTypeStack: {
                ASStackLayoutSpec *s = [[ASStackLayoutSpec alloc] init];
                s.direction = [layout.horizontal boolValue] ? ASStackLayoutDirectionHorizontal : ASStackLayoutDirectionVertical;
                s.spacing = (CGFloat) [layout.spacing floatValue];
                s.justifyContent = stackJustifyContentFrom(layout);
                s.alignItems = stackAlignItemsFrom(layout);
                s.children = children(cs);
                return s;
            }
                
            case CtwLayoutTypeInset: {
                ASInsetLayoutSpec *s = [[ASInsetLayoutSpec alloc] init];
                s.insets = insets(layout);
                s.child = [CTWONodeFactory nodeFrom:onlyChild];
                return s;
            }
                
            case CtwLayoutTypeOverlay: {
                ASOverlayLayoutSpec *s = [[ASOverlayLayoutSpec alloc] init];
                s.child = [CTWONodeFactory nodeFrom:onlyChild];
                s.overlay = node;
                return s;
            }
                
            case CtwLayoutTypeBackground: {
                ASBackgroundLayoutSpec *s = [[ASBackgroundLayoutSpec alloc] init];
                s.child = [CTWONodeFactory nodeFrom:onlyChild];
                s.background = node;
                return s;
            }
                
            case CtwLayoutTypeCenter: {
                ASCenterLayoutSpec *s = [[ASCenterLayoutSpec alloc] init];
                s.centeringOptions = centerCenteringOptions(layout);
                s.sizingOptions = centerSizingOptions(layout);
                s.child = [CTWONodeFactory nodeFrom:onlyChild];
                return s;
            }
                
            case CtwLayoutTypeNone:
            default:
                break;
        }
        return [[ASLayoutSpec alloc] init];
    };
}





#pragma mark Layout Validation

NSString* layoutErrorMsg(NSString *body, id o)
{
    return [NSString stringWithFormat:@"\n\n--- Validation Error ---\n%@\n\n------------------------------------------------\n%@\n------------------------------------------------\n\n", body, o];
}

+ (BOOL) validateLayoutFor:(CtwNode *) data
{   // NOTE: NSAssert must be 'false' to throw
    
    NSString *layoutType = layoutTypeName(data.layout.type);
    
    NSAssert(   (data.layout.type != CtwLayoutTypeStack)
             || (data.children.count > 0),
             layoutErrorMsg([NSString stringWithFormat:@"LayoutType::%@ must have 1 or more children.", layoutType], data));
    
    NSAssert(   (data.layout.type == CtwLayoutTypeStack)
             || (data.layout.type == CtwLayoutTypeNone)
             || (data.children.count == 1),
                layoutErrorMsg([NSString stringWithFormat:@"LayoutType::%@ must have exactly 1 child. Instead, it has %ld children.", layoutType, (unsigned long)data.children.count], data));
    
    NSAssert(   (data.layout.type != CtwLayoutTypeNone)
             || (data.children.count == 0),
             layoutErrorMsg([NSString stringWithFormat:@"LayoutType::%@ cannot have children. It's %ld children would not be displayed.", layoutType, (unsigned long)data.children.count], data));
    
    NSAssert(   (data.type != CtwNodeTypeCollection)
             || (data.layout.type == CtwLayoutTypeNone),
             layoutErrorMsg([NSString stringWithFormat:@"NodeType::COLLECTION must have LayoutType::%@. Apply layout options to the children themselves.", layoutType], data));
    
    return true;
}

@end

