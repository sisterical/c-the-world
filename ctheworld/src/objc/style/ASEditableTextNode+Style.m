//
//  ASEditableTextNode+Style.m
//  libctheworld
//
//  Created by Adam Tait on 9/6/18.
//

#import "ASEditableTextNode+Style.h"
#import "ASDisplayNode+Style.h"
#import "NSAttributedString+Style.h"


@implementation ASEditableTextNode (Style)

- (void) style:(CtwStyle *) style
{
    [self apply:style];
    
    NSString *s = self.attributedText.string;
    self.attributedText = [[NSAttributedString alloc] initWithString:s style:style];
}

@end
