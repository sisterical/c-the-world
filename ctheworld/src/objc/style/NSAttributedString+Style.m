//
//  NSAttributedString+Style.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import "NSAttributedString+Style.h"
#import "CtwStyle+UIKit.h"
#import "CtwColor+UIKit.h"


@implementation NSAttributedString (Style)


- (instancetype)initWithString:(NSString *) str
                         style:(CtwStyle *) style
{
    NSDictionary<NSAttributedStringKey, id> *attrs = @{
                                                       NSFontAttributeName              : [style uiFont],
                                                       NSForegroundColorAttributeName   : [style.fgColor uiColor],
                                                       NSBackgroundColorAttributeName   : [style.bgColor uiColor]
                                                       };
    
    return [[NSAttributedString alloc] initWithString:str
                                           attributes:attrs];
}

@end
