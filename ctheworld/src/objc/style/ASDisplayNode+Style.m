//
//  ASDisplayNode+Style.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import "ASDisplayNode+Style.h"
#import "CtwColor+UIKit.h"


@implementation ASDisplayNode (Style)

- (void) style:(CtwStyle *) style
{
    [self apply:style];
}

- (void) apply:(CtwStyle *) style
{
    // NOTE: all props of CtwStyle are non-null
    self.backgroundColor = [style.bgColor uiColor];
    self.tintColor = [style.fgColor uiColor];
    self.borderColor = [style.borderColor uiColor].CGColor;
    
    self.borderWidth = (CGFloat) style.borderWidth;
    self.cornerRadius = (CGFloat) style.cornerRadius;
    
    // TODO CtwStyle.alpha is not a valid property (yet)
//    self.alpha = (CGFloat) style.alpha;
}

@end
