//
//  CtwColor+UIKit.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import "CtwColor+UIKit.h"

@implementation CtwColor (UIKit)

- (UIColor *) uiColor
{
    return [UIColor colorWithRed:self.r / 255.0
                           green:self.g / 255.0
                            blue:self.b / 255.0
                           alpha:self.alpha];
}

@end
