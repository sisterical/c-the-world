//
//  ASTextNode+Style.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwStyle.h"


@interface ASTextNode (Style)

- (void) style:(CtwStyle *) style;

@end
