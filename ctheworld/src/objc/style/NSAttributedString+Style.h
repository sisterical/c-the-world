//
//  NSAttributedString+Style.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <Foundation/Foundation.h>
#import "CtwStyle.h"
#import "CtwLayout.h"


@interface NSAttributedString (Style)

- (instancetype)initWithString:(NSString *) str
                         style:(CtwStyle *) style;

@end
