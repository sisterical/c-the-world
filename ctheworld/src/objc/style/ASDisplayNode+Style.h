//
//  ASDisplayNode+Style.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwStyle.h"


@interface ASDisplayNode (Style)

- (void) style:(CtwStyle *) style;

// protected - intended for inheritance
- (void) apply:(CtwStyle *) style;

@end
