//
//  ASEditableTextNode+Style.h
//  libctheworld
//
//  Created by Adam Tait on 9/6/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwStyle.h"


// NOTE: ASEditableTextNode required a separate category from
//  ASTextNode because their only common ancestor is ASDisplayNode

@interface ASEditableTextNode (Style)

- (void) style:(CtwStyle *) style;

@end
