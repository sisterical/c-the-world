//
//  ASTextNode+Style.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <Foundation/Foundation.h>
#import "ASTextNode+Style.h"
#import "ASDisplayNode+Style.h"
#import "NSAttributedString+Style.h"



@implementation ASTextNode (Style)

- (void) style:(CtwStyle *) style
{
    [self apply:style];
    
    NSString *s = self.attributedText.string;
    self.attributedText = [[NSAttributedString alloc] initWithString:s style:style];
}

@end
