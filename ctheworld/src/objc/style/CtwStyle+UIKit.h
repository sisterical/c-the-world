//
//  CtwStyle+UIKit.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <UIKit/UIKit.h>
#import "CtwStyle.h"

@interface CtwStyle (UIKit)

- (UIFont *) uiFont;

@end
