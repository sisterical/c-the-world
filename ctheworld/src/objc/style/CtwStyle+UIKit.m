//
//  CtwStyle+UIKit.m
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import "CtwStyle+UIKit.h"

@implementation CtwStyle (UIKit)

- (UIFont *) uiFont
{
    UIFont *f = [UIFont fontWithName:self.fontName
                                size:self.fontSize];
    
    NSAssert1(f != nil, @"ERROR: Font with name %@ is not available.", self.fontName);
    return f;
}

@end
