//
//  CtwColor+UIKit.h
//  libctheworld
//
//  Created by Adam Tait on 8/15/18.
//

#import <UIKit/UIKit.h>
#import "CtwColor.h"

@interface CtwColor (UIKit)

- (UIColor *) uiColor;

@end
