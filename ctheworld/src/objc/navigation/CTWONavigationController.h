//
//  CTWONavigationController.h
//  libctheworld
//
//  Created by Adam Tait on 8/14/18.
//

#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwNavigationController.h"


@interface CTWONavigationController : ASNavigationController <CtwNavigationController>

- (instancetype _Nullable) init;
- (void) next:(nonnull CtwNode *)root;
- (void)open:(nonnull NSString *)url;

@end
