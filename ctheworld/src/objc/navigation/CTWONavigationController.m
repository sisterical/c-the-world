//
//  CTWONavigationController.m
//  libctheworld
//
//  Created by Adam Tait on 8/14/18.
//

#import "CTWONavigationController.h"
#import "CTWONodeFactory.h"


@implementation CTWONavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (instancetype _Nullable) init
{
    if (!(self = [super initWithNibName:nil bundle:nil]))
        return nil;
    
    [self setNavigationBarHidden:true];
    
    return self;
}


- (void) next:(nonnull CtwNode *)root
{
    ASViewController *vc = [CTWONodeFactory viewControllerFrom:root];
    [self pushViewController:vc animated:false];
    [self setNavigationBarHidden:true];               // in case last VC made it visible
}

- (void)open:(nonnull NSString *)url
{
  [UIApplication.sharedApplication openURL:[NSURL URLWithString:url]
                                   options:@{}
                         completionHandler:nil];
}

@end
