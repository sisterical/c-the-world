//
//  CTWOCollectionNode.h
//  libctheworld
//
//  Created by Adam Tait on 8/10/18.
//


#import <AsyncDisplayKit/AsyncDisplayKit.h>
#import "CtwNode.h"


@interface CTWOCollectionNode : ASCollectionNode

- (instancetype _Nonnull) initWithCtwNode:(CtwNode *_Nonnull) ctwNode;

@end
