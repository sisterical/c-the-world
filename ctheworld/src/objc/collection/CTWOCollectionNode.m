//
//  CTWOCollectionNode.m
//  libctheworld
//
//  Created by Adam Tait on 8/10/18.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CTWOCollectionNode.h"
#import "CtwCollectionDataSource.h"
#import "CTWONodeFactory.h"
#import "ASDisplayNode+Style.h"
#import "ASDisplayNode+Layout.h"
#import "ASTextNode+Layout.h"
#import "CtwDimensionType.h"




@interface CTWOCollectionNode () <ASCollectionDataSource, ASCollectionDelegate>
{
    CtwNode *_data;
}

@end


@implementation CTWOCollectionNode

- (instancetype _Nonnull) initWithCtwNode:(CtwNode *) ctwNode
{
    if (!(self = [super initWithCollectionViewLayout:[[UICollectionViewFlowLayout alloc] init]]))
        return nil;
    
    
    _data = ctwNode;
    
    self.dataSource = self;
    self.delegate = self;
    
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    return self;
}



#pragma mark ASCollectionDataSource

- (ASCellNodeBlock)collectionNode:(ASCollectionNode *)collectionNode
      nodeBlockForItemAtIndexPath:(NSIndexPath *)indexPath
{
    (void)collectionNode;
    CGFloat fullWidth = self.view.frame.size.width;
    
    return ^{
        CtwNode *data = [_data.collectionDataSource node:indexPath.row];
        ASCellNode * n = [CTWONodeFactory cellNodeFrom:data];

        // set width in points. ASDimension.fraction does not work on ASCellNode's.
        if (data.layout.width.type == CtwDimensionTypeFraction
            && data.layout.width.value >= 1.0)
        {
            n.style.width = ASDimensionMakeWithPoints(fullWidth);
        }
        
        return n;
    };
}

- (NSInteger)numberOfSectionsInCollectionNode:(ASCollectionNode *)collectionNode
{
    (void)collectionNode;
    return 1;
}

- (NSInteger)collectionNode:(ASCollectionNode *)collectionNode numberOfItemsInSection:(NSInteger)section
{
    (void)collectionNode;
    (void)section;
    
    return [_data.collectionDataSource count];
}



#pragma mark ASCollectionDelegate

- (void)collectionNode:(ASCollectionNode *)collectionNode willBeginBatchFetchWithContext:(ASBatchContext *)context
{
    (void)collectionNode;
    (void)context;
    
    [context completeBatchFetching:YES];
}

@end
