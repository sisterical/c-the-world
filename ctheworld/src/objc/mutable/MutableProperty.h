//
//  MutableProperty.h
//  libctheworld
//
//  Created by Adam Tait on 8/16/18.
//

#import <Foundation/Foundation.h>
#import "Observable.h"


@interface MutableProperty<__covariant T> : Observable<T>

- (instancetype) initWith:(T) value;
- (T) get;
- (void) set:(T) value;

@end
