//
//  Observable.h
//  libctheworld
//
//  Created by Adam Tait on 8/19/18.
//

#import <Foundation/Foundation.h>

@interface Observable<__covariant T> : NSObject

- (instancetype) init;
- (void) addObserver:(void (^)(T)) block;
- (void) notifyAllWith:(id) o;

@end
