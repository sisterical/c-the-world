//
//  Observable.m
//  libctheworld
//
//  Created by Adam Tait on 8/19/18.
//

#import "Observable.h"

@implementation Observable {
    NSMutableArray *_observers;
}

- (instancetype) init
{
    if (!(self = [super init]))
        return nil;
    
    _observers = [[NSMutableArray alloc] init];
    
    return self;
}

- (void) addObserver:(void (^)(id))block
{
    [_observers addObject:block];
}

- (void) notifyAllWith:(id) o
{
    for ( void(^ob)(id) in _observers )
    {
        ob(o);
    }
}

@end

