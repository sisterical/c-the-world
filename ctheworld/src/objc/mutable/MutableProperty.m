//
//  MutableProperty.m
//  libctheworld
//
//  Created by Adam Tait on 8/16/18.
//

#import "MutableProperty.h"

@implementation MutableProperty {
    NSMutableArray *_history;
}

- (instancetype) initWith:(id)value
{
    if (!(self = [super init]))
        return nil;
    
    _history = [[NSMutableArray alloc] initWithObjects:value, nil];
    
    return self;
}

- (id) get
{
    return [_history lastObject];
}

- (void) set:(id) value
{
    [_history addObject:value];
    [self notifyAllWith:value];
}

@end
