//
//  navigation_controller_mock.hpp
//  libctheworld
//
//  Created by Adam Tait on 7/26/18.
//

#ifndef navigation_controller_mock_hpp
#define navigation_controller_mock_hpp

#include "navigation_controller.hpp"

namespace sisterical_test
{
    
    class NavigationControllerMock : public ctheworld_gen::NavigationController
    {
    public:
        NavigationControllerMock() {}
        void next(const ctheworld_gen::Node & root) override;
        void open(const std::string & url) override;
    };
}

#endif /* navigation_controller_mock_hpp */
