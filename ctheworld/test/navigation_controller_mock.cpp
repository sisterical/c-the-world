//
//  navigation_controller_mock.cpp
//  libctheworld
//
//  Created by Adam Tait on 7/26/18.
//

#include "navigation_controller_mock.hpp"

namespace sisterical_test
{
  void NavigationControllerMock::next(const ctheworld_gen::Node & root)   { (void) root; }
  void NavigationControllerMock::open(const std::string & url)            { (void) url; };
}
