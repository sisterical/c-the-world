//
//  scroll_node.cpp
//  libctheworld
//
//  Created by Adam Tait on 8/20/18.
//

#include "scroll_node.hpp"
#include "int_event_generator.hpp"
#include "string_event_generator.hpp"
#include "factory_dimension.hpp"
#include "factory_generator.hpp"
#include "factory_layout.hpp"
#include "factory_node.hpp"
#include "collection_data_source.hpp"
#include "form_node.hpp"


namespace sisterical
{
    using std::string;
    using std::to_string;
    using std::vector;
    using std::make_shared;
    using std::experimental::nullopt;
    using std::experimental::optional;
    
    using ctheworld_gen::NodeType;
    using ctheworld_gen::Style;
    using ctheworld_gen::Color;
    using ctheworld_gen::Layout;
    using ctheworld_gen::LayoutType;
    using ctheworld_gen::LayoutCenterAxisType;
    using ctheworld_gen::LayoutCenterSizingType;
    using ctheworld_gen::Insets;
    
    using ctheworld::IntEventGenerator;
    using ctheworld::StringEventGenerator;
    using ctheworld::CollectionDataSource;
    
    using ctheworld::factory::gen;
    using ctheworld::factory::node;
    using ctheworld::factory::layout;
    using ctheworld::factory::dim;
    
    
    
    
    Node ScrollNode::create(shared_ptr<NavigationController> nav)
    {
        Color black = Color(0, 0, 0, 1);
        Color white = Color(255, 255, 255, 1);
        Color teal = Color(0, 204, 204, 1);
        
        string fontName = "HelveticaNeue-Bold";
        Style textStyle = Style(white, teal,
                                fontName, 18.0,
                                white, 0.0, 0.0);
        Style btnStyle = Style(teal, white,
                               fontName, 18.0,
                               white, 0.0, 0.0);
        Style style = Style(white, black,
                            fontName, 25.0,
                            white, 0.0, 0.0);
        
        shared_ptr<IntEventGenerator> touches = gen::i([nav](int64_t i) {
            nav->next(FormNode::create(nav));
        });
        
        vector<Node> children = vector<Node>();
        for (int i=0; i < 50; i++)
        {
            Node n = node::basic(btnStyle,
                                 layout::stackSpaced(dim::fraction(1.0),
                                                     dim::points(150),
                                                     true,
                                                     5.0),
                                 touches,
                                 {node::text("Content #" + to_string(i+1),
                                             textStyle,
                                             layout::center(dim::fraction(0.6),
                                                            dim::points(20))),
                                  node::image("ImageName",
                                              textStyle,
                                              layout::none(dim::points(150), dim::points(90)))
                                 });
            children.push_back(n);
        }
        
        return node::scroll(style,
                            layout::stackSpaced(dim::fraction(1.0),
                                                dim::points(80),
                                                20),
                            children);
    }
}
