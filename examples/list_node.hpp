//
//  list_node.hpp
//  libctheworld
//
//  Created by Adam Tait on 7/26/18.
//

#ifndef list_node_hpp
#define list_node_hpp

#include "node.hpp"
#include "navigation_controller.hpp"


namespace sisterical
{
  using std::shared_ptr;
  using ctheworld_gen::Node;
  using ctheworld_gen::NavigationController;
    
  class ListNode
  {
  public:
    static Node create(shared_ptr<NavigationController> nav);
  };
}


#endif /* list_node_hpp */
