//
//  scroll_node.hpp
//  libctheworld
//
//  Created by Adam Tait on 8/20/18.
//

#ifndef scroll_node_hpp
#define scroll_node_hpp

#include "node.hpp"
#include "navigation_controller.hpp"


namespace sisterical
{
    using std::shared_ptr;
    using ctheworld_gen::Node;
    using ctheworld_gen::NavigationController;
    
    class ScrollNode
    {
    public:
        static Node create(shared_ptr<NavigationController> nav);
    };
}


#endif /* scroll_node_hpp */
