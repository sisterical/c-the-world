//
//  list_node.cpp
//  libctheworld
//
//  Created by Adam Tait on 7/26/18.
//

#include "list_node.hpp"
#include "int_event_generator.hpp"
#include "string_event_generator.hpp"
#include "factory_dimension.hpp"
#include "factory_generator.hpp"
#include "factory_layout.hpp"
#include "factory_node.hpp"
#include "collection_data_source.hpp"
#include "scroll_node.hpp"


namespace sisterical
{
  using std::make_shared;
  using std::experimental::nullopt;
  using std::experimental::optional;

  using ctheworld_gen::NodeType;
  using ctheworld_gen::Style;
  using ctheworld_gen::Color;
  using ctheworld_gen::Layout;
  using ctheworld_gen::LayoutType;
  using ctheworld_gen::LayoutCenterAxisType;
  using ctheworld_gen::LayoutCenterSizingType;
  using ctheworld_gen::Insets;

  using ctheworld::IntEventGenerator;
  using ctheworld::StringEventGenerator;
  using ctheworld::CollectionDataSource;
    
  using ctheworld::factory::gen;
  using ctheworld::factory::node;
  using ctheworld::factory::layout;
  using ctheworld::factory::dim;
    
    
    
    
  Node ListNode::create(shared_ptr<NavigationController> nav)
  {
    Color black = Color(0, 0, 0, 1);
    Color white = Color(255, 255, 255, 1);
    Color teal = Color(0, 204, 204, 1);
        
    Style textStyle = Style(white, teal,
                            "Helvetica", 18.0,
                            white, 0.0, 0.0);
    Style btnStyle = Style(teal, white,
                           "Helvetica", 18.0,
                           white, 0.0, 0.0);
    Style style = Style(white, black,
                        "Helvetica", 25.0,
                        white, 2.0, 0.0);
      
    shared_ptr<IntEventGenerator> touches = gen::i([nav](int64_t i) {
        nav->next(ScrollNode::create(nav));
      });
      
    CollectionDataSource ds = CollectionDataSource(100, [btnStyle, textStyle, touches]
                                                   (int64_t i) -> Node
                                                   {
                                                     return node::basic(btnStyle,
                                                                        layout::center(dim::fraction(1.0),
                                                                                       dim::points(100)),
                                                                        {node::basic(btnStyle,
                                                                                     layout::center(dim::fraction(1.0),
                                                                                                    dim::fraction(1.0)),
                                                                                     touches,
                                                                                     {node::text("Cell " + std::to_string(i),
                                                                                                 textStyle,
                                                                                                 layout::none())})});
                                                   });
    Node collection = node::collection(style,
                                       layout::none(),
                                       make_shared<CollectionDataSource>(ds));


    return node::basic(style,
                       layout::center(),
                       {collection});;
  }
}
