//
//  form_node.cpp
//  libctheworld
//
//  Created by Adam Tait on 9/6/18.
//

#include <stdio.h>
#include "form_node.hpp"
#include "int_event_generator.hpp"
#include "string_event_generator.hpp"
#include "factory_dimension.hpp"
#include "factory_generator.hpp"
#include "factory_layout.hpp"
#include "factory_node.hpp"
#include "collection_data_source.hpp"


namespace sisterical
{
    using std::string;
    using std::to_string;
    using std::vector;
    using std::make_shared;
    using std::experimental::nullopt;
    using std::experimental::optional;
    
    using ctheworld_gen::NodeType;
    using ctheworld_gen::Style;
    using ctheworld_gen::Color;
    using ctheworld_gen::Layout;
    using ctheworld_gen::LayoutType;
    using ctheworld_gen::LayoutCenterAxisType;
    using ctheworld_gen::LayoutCenterSizingType;
    using ctheworld_gen::Insets;
    
    using ctheworld::IntEventGenerator;
    using ctheworld::StringEventGenerator;
    using ctheworld::CollectionDataSource;
    
    using ctheworld::factory::gen;
    using ctheworld::factory::node;
    using ctheworld::factory::layout;
    using ctheworld::factory::dim;
    
    
    
    
    Node FormNode::create(shared_ptr<NavigationController> nav)
    {
        Color black = Color(0, 0, 0, 1);
        Color white = Color(255, 255, 255, 1);
        Color teal = Color(0, 204, 204, 1);
        
        string fontName = "HelveticaNeue-Bold";
        Style textStyle = Style(white, teal,
                                fontName, 28.0,
                                white, 0.0, 0.0);
        Style btnStyle = Style(teal, white,
                               fontName, 18.0,
                               white, 0.0, 0.0);
        Style style = Style(white, black,
                            fontName, 25.0,
                            white, 0.0, 0.0);
        
        shared_ptr<StringEventGenerator> textChanges = gen::s([nav](string s) {
            printf("new text: [%s]\n", s.c_str());
        });
        
        
        return node::basic(style,
                           layout::stackSpaced(dim::fraction(1.0),
                                               dim::fraction(1.0),
                                               20),
                           {node::text("Editable text area",
                                       textStyle,
                                       layout::center(dim::fraction(0.6),
                                                      dim::points(50))),
                               
                           node::text("no placeholder",
                                      textStyle,
                                      layout::center(dim::fraction(0.9),
                                                     dim::points(300)),
                                      textChanges),
                           });
    }
}
