//
//  form_node.hpp
//  libctheworld
//
//  Created by Adam Tait on 9/6/18.
//

#ifndef form_node_hpp
#define form_node_hpp

#include "node.hpp"
#include "navigation_controller.hpp"

namespace sisterical
{
    using std::shared_ptr;
    using ctheworld_gen::Node;
    using ctheworld_gen::NavigationController;
 
    
    class FormNode
    {
    public:
        static Node create(shared_ptr<NavigationController> nav);
    };
}

#endif /* form_node_hpp */
