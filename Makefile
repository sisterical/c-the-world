#  C-the-World Makefile
#

all: ios
.PHONY: all


# - Dependencies

./deps/gyp/gyp:
	git submodule update --init ./deps/gyp
gyp: ./deps/gyp/gyp
.PHONY: gyp


./deps/djinni/src/run:
	git submodule update --init ./deps/djinni
djinni: ./deps/djinni/src/run
.PHONY: djinni

./Carthage/Build/iOS/AsyncDisplayKit.framework:
	./script/build/carthage-update.sh
ios_frameworks: ./Carthage/Build/iOS/AsyncDisplayKit.framework
.PHONY: ios_frameworks



# - Djinni generated-src

./ctheworld/generated-src: djinni
	./script/build/idl.sh build ctheworld/idl ctheworld/generated-src


# - Djinni generated-src (extensions)



# - Project files

build_ios/app.xcodeproj: gyp app.gyp ios_frameworks ./ctheworld/generated-src
	PYTHONPATH=deps/gyp/pylib deps/gyp/gyp app.gyp -DOS=ios --depth=. -f xcode --generator-output=./build_ios -Icommon.gypi



# - Binaries

xb-prettifier := $(shell command -v xcpretty >/dev/null 2>&1 && echo "xcpretty -c" || echo "cat")

ios: build_ios/app.xcodeproj
	xcodebuild -project build_ios/app.xcodeproj -configuration Release -target app | ${xb-prettifier}
.PHONY: ios



# - Helpers

clean:
	rm -rf build
	rm -rf build_ios
	./script/build/idl.sh clean ctheworld/generated-src
.PHONY: clean


pretty_gyp: ./deps/gyp app.gyp common.gypi
	deps/gyp/tools/pretty_gyp.py app.gyp > .app.gyp && mv .app.gyp app.gyp
	deps/gyp/tools/pretty_gyp.py common.gypi > .common.gypi && mv .common.gypi common.gypi
.PHONY: pretty_gyp
