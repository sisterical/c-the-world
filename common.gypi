{
  'configurations': {
    'Debug': {
      'xcode_settings': {
        'GCC_OPTIMIZATION_LEVEL': '0',
        'ONLY_ACTIVE_ARCH': 'YES',
      },
    },
  },

  'target_defaults': {
    'default_configuration': 'Debug',
    'cflags': ['-Wall', '-Wextra', '-Werror', '-fvisibility=hidden', '-Wunused-parameter'],
    'cflags_cc': [ '<@(_cflags)', '-std=c++1z', '-fexceptions', '-frtti' ],
    'xcode_settings': {
      'OTHER_CFLAGS' : ['<@(_cflags)'],
      'OTHER_CPLUSPLUSFLAGS' : ['<@(_cflags_cc)'],
      'CLANG_CXX_LANGUAGE_STANDARD': 'gnu++14',
      'CLANG_CXX_LIBRARY': 'libc++',
      'CLANG_ENABLE_OBJC_ARC': 'YES',
      'TARGETED_DEVICE_FAMILY': "1,2",
      'SWIFT_VERSION': '4.0',
      'CODE_SIGN_IDENTITY': "iPhone Developer",
      'CODE_SIGN_STYLE': "Automatic",
      'IPHONEOS_DEPLOYMENT_TARGET': "10.13",   # <- wasn't able to get building with 11+ working
      #'DEVELOPMENT_TEAM': "********",     # <- You may want to add your own development team for code signing
    },
    'conditions': [
      ['OS=="ios"',
        {
          'xcode_settings' : {
            'SDKROOT': 'iphoneos',
            'SUPPORTED_PLATFORMS': 'iphonesimulator iphoneos',
          },
        }
      ],
      ['OS=="mac"',
        {
          'xcode_settings+' : {
            'SDKROOT': 'macosx',
          },
        }
      ],
    ],
    'configurations': {
      'Debug': {
        'defines': [ 'DEBUG' ],
        'cflags' : [ '-g', '-O0' ],
        'xcode_settings' : {
          'GCC_OPTIMIZATION_LEVEL': '0',
          'ONLY_ACTIVE_ARCH': 'YES',
        },
      },
      'Release': {
        'defines': [ 'NDEBUG' ],
        'cflags': [
          '-Os',
          '-fomit-frame-pointer',
          '-fdata-sections',
          '-ffunction-sections',
        ],
        'xcode_settings': {
          'DEAD_CODE_STRIPPING': 'YES',
        },
      },
    },
  },
}
