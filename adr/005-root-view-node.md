# Architecture Decision Record 005

## Root View Node


### Context

Once we've created our immutable view node structure, specifically
defined each view node and it's interactivity (through events), then
we need to understand where to hang the root view node so that the
view node structure becomes live (available to the user).


#### View Controllers

The popular abstraction for encapsulating a set of view nodes and that
local view state is a View Controller. MVC (model, view, controller)
is the abstraction that iOS's UIKit was built on. Android has a
similar idea called Activiy. We're not dealing with models in this set
of tools (you can do that yourself), but views & controllers or
activities are in our domain.

View Controllers & Activities often have:

+ root view node, including subnodes
+ understanding of or access to the view lifecycle
+ local state for view tree
+ handlers for interactivity events on view tree (gestures in iOS or actions in Android)
+ functions for updating/changing the view tree

It's no wonder developers are always pinning code into View
Controllers, then complaining about how large and bloated they get.
View Controllers have many (too many?) responsibilities. We have an
opportunity to reduce these responsibilities, or at least spread them
out.


##### Reference to the root view node

Given that these view nodes are all immutable (ADR #3), we can just
generate a new view node (or view node tree) whenever we need one. We
won't need to jump into the existing tree and change it, we can't in
fact (it's immutable). Instead, we can get away with a single root
(live!) view node for the entire app. Updates will just replace that
single root view node with a new root view node (which will be done in
an efficient way).

So, we don't need a View Controller to hold the root view node anymore.


##### View lifecycle

If we replace the app-wide root view node with another at anytime, and
rely on the system to display it quickly, then we don't need to reach
into the view lifecycle anymore.


##### Local State

We do need to hang local state variables somewhere. Let's address this
further on.


##### Handlers for user interaction callbacks

With handlers hanging off references on the view node, we don't need
those handlers on a separate object (like a View Controller).

We run into challenges when the handlers need access to local state.
We still need a solution for storing state but there are (at least)
three ways our event listening functions could access that shared
state:

+ app has global/system shared state which handler function could access easily
+ handlers are functions, so we could curry the reference to their dependent shared state
+ handlers are objects that support dependency injection, so they are passed references to the shared state

We will not force the developer to use any one of these options,
leaving the choice open. Different developers in different
environments may make different trade-offs.


#### Navigation Controllers

iOS and Android have a mechanism for navigation between View
Controllers or Activities. This mechanism is usually app-wide (you
wouldn't be likely to have more than one Navigation Controller) and
would manage individual views and their lifecycles. It's interface
might look like:

+ get/set appearance properties (example: visible or hidden)
+ get/set behavior properties (example: always transition with a slide animation)
+ get/set root view node
+ transition to a new root view node

Since view nodes will be defined in our tools, we'll need access to
each platform's navigation through an interface.



### Decision

ctheworld will not have the concept of a View Controller or an
Activity or a lifecycle. Instead, it will define view nodes in such a
way that:

+ view nodes will be immutable and any mutation will require a complete re-creation from the root view node, on down the tree
+ view nodes will require interaction callbacks in their definitions

To achieve this, ctheworld will need access to update the root view
node. This access will be provided by the platform through an
interface. That interface will be similar to a navigation controller.

The navigation controller interface will provide:

+ set root view node
+ update root view node



### Status

Accepted.


### Consequences

Managing both local and shared state will be left up to the developer.
They may decide to make their own trade-offs.
