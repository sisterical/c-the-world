# Architecture Decision Record 000

## Template


### Context

We are building software. We are exploring a new space or approach as
we work. Along the way, we will be making decisions about the
direction of the project.

### Decision

We will write an ADR for each significant decision to the architecture
of the project.


### Status

Accepted. [Proposed. Superceded. Deprecated.]


### Consequences

This project will include a set of ADRs, describing the decisions in
the order that they were made.

We will be careful about choosing the decisions that require an ADR to
be written, selecting only those that have a significant impact on the
project. This is an art to be practiced and considered.

There is a risk to the timeline of the project by writing ADRs, since
considering our decisions and writing about them takes time. We
believe the value of having the historical record of ADRs outweighs
the time required in the long run.
