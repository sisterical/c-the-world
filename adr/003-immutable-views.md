# Architecture Decision Record 003

## Immutable Views


### Context

On of my favorite languages is Clojure, which emphasizes immutable
data structures. I've seen great value in other contexts with this
approach. Indeed, it's the same concept that the popular React
framework is based on.

Immutable structures should make holding a mental model of changes to
views easier. Rather than the state of the views & structure being
dynamic, it should be easier to know

+ what views are being replaced
+ when those views are being replaced/displayed
+ what the current state of the view structure is

Others have done studies on the performance implications of the
immutable view approach, specifically with React JS. Generally, those
results are similar to the standard immutable models though with more
garbage collection. Performance is not the most important goal here
(ease of working with the models is) though I hope for there to be no
sacrifice. Most of the performance implications for the C-the-world
will be pushed down to the native layer, for which you can use
AsyncDisplayKit/Texture on iOS, React JS on web or the equivalent on
Android.


### Decision

Views, view structures, layout properties and style properties will be
defined in immutable data structures.


### Status

Accepted.


### Consequences

Immutable data structures presents a couple challenges; performance of
memory required for the data structures, making updates to the data
structures, and performance of the tools for making those updates.


#### Memory use

Let's punt on memory issues until they arise. Data structure re-use
can be done in code (variables) and it's likely that individual data
structures in a UI application will get re-used so seldomly that
duplication is unlikely to be a significant problem.

#### How to make updates to immutable data structures

Making updates should be straightforward. There'll be a clear point of
mutability (like the root node to be displayed) and the code will
replace one immutable data structure with the other.


#### Performance of updates to immutable data structures

Updates with immutable data structures become a problem when the data
structures get really big or the data structure gets a lot of updates.
Both could happen to our view structures.

Fortunately, AsyncDisplayKit/Texture on iOS solves this problem by
pruning the tree down to just the nodes that have changed and updating
those. I assume that we'll be able to find similar tools (based on
ReactJS) on Android.

We may eventually need a better answer to managing large immutable
data structures in memory.
