# Architecture Decision Record 001

## Cross Platform


### Context

I'm a mobile app developer. My previous work has been exclusively on
iOS, but I have plans to build for Android also.

When I was considering building an app to target both iOS & Android, I
did some research into how others were sharing work/code. I found
these answers:

+ Facebook's React Native
+ Google's Flutter
+ QT
+ Xamarin
+ RubyMotion
+ Dropbox's Djinni
+ Homegrown code generating tools
+ not shared; 2+ separate codebases



#### Priorities

Each option has it's own tradeoffs, so I needed a set of values to
guide my decision. Here are my values in building user interfaces for
mobile apps:

+ flexible bridge between tool & native code. Allows me drop into native code when I want.
+ easy to compose views together, preferably in a declarative DSL.
+ easy to describe style attributes and layout attributes, and relate to the views they apply to.
+ flexible control for platform specific features. If I want to implement a header in a unique way on each platform, I should be able to do that.
+ concurrency constructs available. Preferrably, CSP-style channels (like Go or Clojure).
+ debugging isn't much more complicated than native code.
+ DSL is native code, not a separate static language.


#### Testing existing open source frameworks for building cross platform apps

I tried React Native first. It had the biggest community and seemed
the most well-baked. Since it runs on a single JavaScript thread, it
has no support for concurrency. It uses a DSL (JSX) for defining view
structure, and maps for style attributes - very similar to HTML & CSS.
However, composing views/components must be done in code. It was based
on ideas from ReactJS, so has app state dependency injection and plays
nicely with immutable data structures (not in values list, but stuff I
care about). It also shares code/libraries with AsyncDisplayKit (my
iOS layout tool of choice), which made running ReactNative in the same
codebase difficult (dependecy conflicts). The debugging story with
React Native wasn't great, either (many levels of indirection).

Next, I tried Flutter. I liked a lot of what Flutter offers like the
declarative description of views and the required isolation of state
changes. However, it doesn't support concurrency (they might in the
future but they have no plans to), I'm not that fond of the Dart
language and I would prefer not to have all of Dart+Flutter's magic
between my code and native code.


Djinni was daunting at first, but seemed to offer the best of the
options. In fact, it's not fair to compare Djinni to RN or Flutter
since Djinni is simply an Interface Definition Language + code
generation tools for C++, ObjC & Java. So, it does only a limited
subset of the work that RN or Flutter do - it's simpler and leaves me
with lots of freedom. C++ wasn't my ideal shared language but it's
grown significantly in the past decade and still has a huge developer
community.



### Decision

The result of the initial research into (above) tools has resulted in
a compound set of decisions. 


#### Framework Trade-offs

I'm sure that React Native & Flutter are working well for Facebook &
Google (and probably a scattering of other teams), but the trade-offs
they make don't suit my needs or priorities. 

I am (currently) a one-man team. I don't mind writing code in several
languages. The risk of debugging through opaque abstractions is high &
painful for me. I need power over my tools and a language that is
simple rather than safe. This unique set of priorities does tend to
push me towards build rather than learn + adopt.

The first decision is that we will build our own tool(s) rather than
adopt one of the (seemingly popular) cross-platform frameworks.


#### Djinni

Djinni impressed by being just simple enough. It solves one important
problem in software that runs on iOS & Android, which is bridging code
to run in both places. That's all it does, and it seems to do it well.

Starting with Djinni gives us a huge leap forward by building the
interface & bridge.

The second decision is that we will use
[Dropbox's Djinni](https://github.com/dropbox/djinni) to bridge C++
code between iOS (ObjC or Swift) and Android (Java via NDK).


### Status

Accepted.


### Consequences

We will be creating a project & git repository for our new tool.

We will call the new tool(s), C The World.

Djinni will be added as a dependency to C The World.

Djinni will be used to define the interface between shared code & native code.

The primary language for shared cross platform code will be C++.

