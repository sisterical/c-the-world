# C-the-World Developer's Guide


## Getting started

Use the _new project creation_ tool.

```bash
# Usage: new-project.sh PROJECT_NAME [DIRECTORY]
./script/tool/new-project.sh app ../new-c-the-world-app
```

With your new project built, continue:

+ Open `build_ios/app.xcodeproj` in XCode.
+ Follow the [XCode & iOS app tips](#xcode--ios-apps)
+ Select the _app_ target
+ Run the iOS simulator
+ Try changing `src/cpp/root_node.cpp`
+ Run, again


Later, you might want take a look at the [examples/](examples/).



## Building

Start by running `make`.

`make` does all this:

+ `git submodule update *`
+ `djinni build idl/`
+ `carthage update`
+ `gyp build ios`
+ `xcodebuild`

All this will result in `build_ios/app.xcodeproj`, which you can open in XCode.



### Warnings and troubleshooting


Note to myself: These build issues are annoying. Hopefully, fixes can
be directly integrated into the GYP build or (in the future) CMake.


#### XCode & iOS apps

If you're having trouble getting your app to build, make sure that
you've done these things:



##### Third party libraries (like Texture/AsyncDisplayKit)

The iOS half of C-the-World is built with
[Texture/AsyncDisplayKit](https://github.com/texturegroup/texture).
C-the-World is configured to download & build Texture/AsyncDisplayKit
using [Carthage](https://github.com/Carthage/Carthage#quick-start) in
the `Makefile`.


###### Linking

Add `AsyncDisplayKit.framework` as a _Linked Frameworks and
Libraries_. Find this on the `General` tab of the `app` target. If
you're using the default _app_.gyp then it's probably already there.
  

###### Archiving  
  
When archiving, you need to make sure that your third party libraries
(Texture/AsyncDisplayKit, included) are correctly linked, copied to
the build products and _not_ code signed.
[Carthage already has a simple guide & script to handle the copy phase correctly; use it.](https://github.com/Carthage/Carthage#quick-start)


###### Adding your own third party libraries

If you're using Carthage, then you can follow the above and do exactly
what's been done for Texture/AsyncDisplayKit. 

Building, linking & signing your own third party libs is possible.
Also, using other package managers (like CocoaPods or the Swift
Package Manager) are also possible, though no support for these
options is currently available. If you work out the kinks, then please
update this README.



##### Asset Catalogs

Put your `.xcassets` under the `/src` subdirectory, otherwise you'll
need to include them manually.

You can manually add your `.xcassets` to your `.gyp` file as follows:

```
'mac_bundle_resources': [
   "<!@(python script/build/glob.py -i [OTHER RESOURCES DIRECTORY PATH]/ *.xcassets)",
]
```

##### XCode Archive Organizer is not recognizing my application as an "iOS App"

A problem can occur after successfully creating an archive using your
application target in XCode, the Archive Organizer does not recognize
your application as an _iOS App_ rather it appears in the _Other_
list.

This is likely caused by some dependent library targets has set
`SKIP_INSTALL=NO` in their build settings. For whatever reason, XCode
will not build an application when more than one library _INSTALLS_.

The solution is to make sure that all your dependent targets include
`SKIPS_INSTALL=YES` in their build settings. This can be done manually
in the _project.pbxproj_ file with the settings for the target or you
can add the following to the target in your _.gyp_ file.

```
"xcode_settings": {
   "SKIP_INSTALL": "YES",
},
```



### Adding Native Extensions

Use the _new extension creation_ tool.

```bash
./script/tool/new-extension.sh {{ new-extension-name }}
```

+ add some [Djinni](https://github.com/dropbox/djinni) idl defintions to `{{extension}}/idl/`
+ run `make` to build `{{extension}}/generated-src/` from idl definitions
+ implement the feature using native code in `{{extension}}/src/objc/`
+ [optional] implement C/C++ factories or helpers in `{{extension}}/src/cpp/`
+ use your new interfaces/factories/helpers in your app code.
