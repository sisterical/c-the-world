{
  'targets': [


    # c-the-world CPP target
    {
      'target_name': 'libctheworld',
      'type': 'static_library',
      'sources': [
        "<!@(python script/build/glob.py ctheworld/src/cpp/ *.h *.hpp *.cpp)",
        "<!@(python script/build/glob.py ctheworld/generated-src/cpp/ *.h *.hpp *.cpp)",
        "<!@(python script/build/glob.py ctheworld/test/ *.h *.hpp *.cpp)",
      ],
      'include_dirs': [
        "ctheworld/src/cpp",
        "ctheworld/generated-src/cpp",
        "ctheworld/test",
      ],
      'all_dependent_settings': {
        'include_dirs': [
          "ctheworld/src/cpp",
          "ctheworld/generated-src/cpp",
          "ctheworld/test",
        ],
        'libraries': [
          "libc++.a",
        ],
      },
    },



    # app target
    {
      'target_name': 'app',
      'product_name': 'app',
      'type': 'executable',
      'mac_bundle': 1,
      'dependencies': [
        "deps/djinni/support-lib/support_lib.gyp:djinni_objc",
        "libctheworld",
      ],
      'sources': [
        "<!@(python script/build/glob.py src/ *.h *.m *.mm *.hpp *.cpp)",
        "<!@(python script/build/glob.py ctheworld/src/objc/ *.h *.m *.mm *.hpp *.cpp)",
        "<!@(python script/build/glob.py ctheworld/generated-src/objc/ *.h *.hpp *.mm *.m)",
      ],
      'mac_bundle_resources': [
        "<!@(python script/build/glob.py src/ *.otf *.ttf *.storyboard)",
        "<!@(python script/build/glob.py -i src/ *.xcassets)",
      ],
      'include_dirs': [
        "src/",
        "ctheworld/src/objc",
        "ctheworld/generated-src/objc",
      ],
      'libraries': [
        "UIKit.framework",
        "Carthage/Build/iOS/AsyncDisplayKit.framework",
      ],
      'mac_framework_dirs': [
        "$(PROJECT_DIR)/Carthage/Build/iOS/",
      ],
      'link_settings': {
        'libraries': [
          "Carthage/Build/iOS/AsyncDisplayKit.framework",
        ]
      },
      'xcode_settings': {
        'INFOPLIST_FILE' : "src/ios/supporting-files/Info.plist",
        'PRODUCT_BUNDLE_IDENTIFIER': 'com.sisterical.ctheworld.app',
      },
    },

  ],
}
