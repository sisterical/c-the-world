//
//  AppDelegate.h
//  libctheworld
//
//  Created by Adam Tait on 8/16/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// WARNING! DO NOT USE/CREATE ANY NEW VARIABLES HERE

@end
