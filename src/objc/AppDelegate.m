//
//  AppDelegate.m
//  libctheworld
//
//  Created by Adam Tait on 8/16/18.
//

#import "AppDelegate.h"
#import "CTWONavigationController.h"
#import "CtwApp.h"



@interface AppDelegate () {
    CTWONavigationController *_nav;
    CtwApp *_app;
}
@end


@implementation AppDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    (void)application;
    (void)launchOptions;
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    _nav = [[CTWONavigationController alloc] init];
    _app = [CtwApp create:_nav];
    
    self.window.rootViewController = _nav;
    [self.window makeKeyAndVisible];
    
    return true;
}

@end
