//
//  root_node.hpp
//  libctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#ifndef root_node_hpp
#define root_node_hpp

#include "node.hpp"
#include "navigation_controller.hpp"
#include "shared_state.hpp"


namespace ctheworld
{
  using std::shared_ptr;
  using ctheworld_gen::Node;
  using ctheworld_gen::NavigationController;
    
    
  class RootNode
  {
  public:
    static Node create(shared_ptr<NavigationController> nav,
                       shared_ptr<SharedState> sharedState);
  };
}


#endif /* root_node_hpp */
