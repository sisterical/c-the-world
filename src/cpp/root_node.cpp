//
//  root_node.cpp
//  libctheworld
//
//  Created by Adam Tait on 6/13/18.
//  Copyright © 2018 Sisterical Inc. All rights reserved.
//

#include "root_node.hpp"
#include "int_event_generator.hpp"
#include "factory_dimension.hpp"
#include "factory_generator.hpp"
#include "factory_layout.hpp"
#include "factory_node.hpp"


namespace ctheworld
{
  using std::string;

  using ctheworld_gen::Style;
  using ctheworld_gen::Color;

  using ctheworld::factory::dim;
  using ctheworld::factory::gen;
  using ctheworld::factory::layout;
  using ctheworld::factory::node;
    
    
  Node
  RootNode::create(shared_ptr<NavigationController> nav,
                   shared_ptr<SharedState> sharedState)
  {
    (void) nav;
    (void) sharedState;
    
    Color black = Color(0, 0, 0, 1);
    Color lightgrey = Color(200, 200, 200, 1);
    Color white = Color(255, 255, 255, 1);
    Color blue = Color(50, 50, 204, 1);
    Color reddish = Color(224, 124, 104, 1);
    
    
    string fontName = "HelveticaNeue-Bold";
    Style headTextStyle = Style(reddish, blue,
                                fontName, 32.0,
                                white, 0.0, 0.0);
    Style subtitleTextStyle = Style(reddish, blue,
                                    fontName, 24.0,
                                    white, 0.0, 0.0);
    Style btnStyle = Style(blue, blue,
                           fontName, 14.0,
                           white, 3.0, 50.0);
    Style style = Style(lightgrey, black,
                        fontName, 14.0,
                        white, 0.0, 00.0);
      
      
    shared_ptr<IntEventGenerator> touches = gen::i([nav](int64_t i) {
        printf("--> Help me do something! I've been touched %lld times.\n", i);
      });
      
      
    Node btnNode = node::basic(btnStyle,
                               layout::stackSpaced(dim::fraction(0.8),
                                                   dim::fraction(0.4),
                                                   0.0),
                               touches,
                               {node::text("My first\nC-the-World\napp!",
                                           headTextStyle,
                                           layout::center(dim::fraction(0.8),
                                                          dim::fraction(0.5))),
                                   node::text("You've only just begun the journey to building better apps.",
                                              subtitleTextStyle,
                                              layout::center(dim::fraction(0.8),
                                                             dim::fraction(0.4)))
                                   });
      
    Node n = node::basic(style,
                         layout::center(),
                         {node::basic(style,
                                      layout::center(),
                                      {btnNode})});
    return n;
  }
}
