#! /usr/bin/env bash

echo -e "\nC-the-World : new project builder"

# --- check usage + argument count
if [ $# == 0 ] || [ $# -gt 2 ]
then
    echo ""
    echo "Usage: new-project.sh PROJECT_NAME [DIRECTORY]"
    echo "example: new-project.sh new-c-the-world-app"    
    echo "example: new-project.sh app ../new-c-the-world-app"
    echo ""
    exit 0
fi


# --- find directories
dest_dirname="$2"
if [ "$dest_dirname" == "" ]; then
    dest_dirname="$1"
fi
dest_dir="$( pwd -P )/$dest_dirname"

hash realpath 2>/dev/null || { echo >&2 "C-the-World script requires \"realpath\" but it's not installed.  Please install it and try again."; exit 1; }
base_dir="$(dirname $(realpath "$0"))"/../..

cd $base_dir



# --- print version
git_hash=`git rev-parse --verify HEAD`
github_address="[github.com/adamtait/ctheworld/commit/$git_hash]"
echo -e "version: $github_address\n"


# --- make destination directory
if [[ ! -d $dest_dir ]]; then
    echo "--> creating $dest_dir"
    mkdir $dest_dir
fi


# --- sync files
echo "--> Sync'ing files to $dest_dir"
to_sync=( ".gitignore"
          "Cartfile"
          "Cartfile.resolved"
          "Makefile"
          "app.gyp"
          "common.gypi"
          "ctheworld"
          "script"
          "src")

for fd in ${to_sync[*]}
do
    echo "----> copying $fd"
    cp -rf "$base_dir/$fd" "$dest_dir/$fd"
done


# --- setup destination directory
cd $dest_dir

# ---- add git repository
if [[ ! -d ".git" ]]; then
    echo -e "--> creating Git repository"
    git init
fi

# ---- add submodules
if [[ ! -d "deps" ]]; then
    echo "--> creating deps (submodule dependencies)"

    mkdir deps
    cd deps

    remotes=( "https://github.com/dropbox/djinni.git"
              "https://chromium.googlesource.com/external/gyp.git")
    for remote in ${remotes[*]}; do
        echo "---> git cloning $remote"
        git submodule add $remote
    done
    
    cd ..
fi


# ---- update Makefile & GYP file with project name
echo "--> updating Makefile & GYP file with project name [$1]"

mv Makefile .Makefile.temp
sed -e "s/app/$1/g" .Makefile.temp > Makefile
sed \
    -e "s/'app'/'$1'/g" \
    -e "s/'com.sisterical.ctheworld.app'/'com.example.$1'/g" \
    app.gyp > "$1.gyp"
rm -f .Makefile.temp app.gyp


# ---- commit new files to local Git repo
echo "--> committing changes to Git"
git add .
git commit -m "C-the-World new project 
$github_address
Created '$1'" 2>&1 >/dev/null


echo -e "\n--> done <--"
echo -e "Enjoy C the World!\n"


# ---- run make
read -p "Would you like to run 'make'?  [y] or [n]: " run_make
if [ "$run_make" = "y" ] || [ "$run_make" = "Y" ]
then
    make
fi
