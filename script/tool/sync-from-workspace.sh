#! /usr/bin/env bash

if (( $# != 1 )); then
    echo "Usage: ./sync-from-workspace.sh {{source directory}}"
    exit 0
fi

base_dir=$(cd "`dirname "0"`" && pwd)
src_dir="$base_dir/../$1"


copy_files () {
    local dirs=("$@")
    for fd in ${dirs[*]}
    do
        echo "copying... $fd"
        cp -rf "$src_dir/$fd" "$base_dir/"
    done
}



echo "Sync'ing from $src_dir"
echo "NOTE: this script doesn't handle changes to submodules."

dirs=("ctheworld" "script")
copy_files ${dirs[*]}



echo ""
read -p 'Would you like to sync build files (Makefile, GYP files, ...)? [y] or [n]: ' sync_build_files

build_files=("Makefile" "*.gyp" "*.gypi")
if [ "$sync_build_files" = "y" ] || [ "$sync_build_files" = "Y" ]; then
    copy_files ${build_files[*]}
fi

echo -e "\n--- done sync'ing."
