#! /usr/bin/env bash

echo -e "\nC-the-World : new extension scaffolding"

# --- check usage + argument count
if [ $# == 0 ] || [ $# -gt 1 ]
then
    echo ""
    echo "Use this script to scaffold a new native extension for a C-the-World project."
    echo "Usage: new-extension.sh EXTENSION_NAME"
    echo "example: new-extension.sh new-extension"
    echo ""
    echo "NOTE: This script assumes that you are creating the native extension in a C-the-World project, at the same level as (Makefile, **name**.gyp)."
    echo ""
    exit 0
fi


# --- find source directory (ctheworld git repo)
hash realpath 2>/dev/null || { echo >&2 "C-the-World script requires \"realpath\" but it's not installed.  Please install it and try again."; exit 1; }
src_dir="$(dirname $(realpath "$0"))"/../..


# --- make destination directory
current_dir="$( pwd -P )"
current_project_name="$(basename "$current_dir")"


if [[ ! -d "$current_dir/ctheworld" ]] || [[ ! -f "$current_dir/$current_project_name.gyp" ]]
then
    echo ""
    echo "You MUST create the native extension in a C-the-World project, at the same level as (Makefile, $current_project_name.gyp)."
    echo "Please move to that directory and run again."
    echo ""
    exit 0
fi


dest_dir="$current_dir/$1"
if [[ ! -d "$dest_dir" ]]; then
    echo "--> Creating $dest_dir"
    mkdir $dest_dir
fi


echo -e "\n---> Creating directory structure"
echo "----> idl/"
mkdir "$dest_dir/idl"
echo "----> src/"
mkdir "$dest_dir/src"
echo "----> src/cpp"
mkdir "$dest_dir/src/cpp"
echo "----> src/objc"
mkdir "$dest_dir/src/objc"
echo "----> test/"
mkdir "$dest_dir/test"



# -- building placeholder files
DATE=`date +%m/%d/%y`
USERNAME=`whoami`

echo_hpp ()
{
    echo -e "//
//  $1.$2
//  $3
//
//  Created by $USERNAME on $DATE.
//

#ifndef $1_$2
#define $1_$2


#endif /* $1_$2 */"
}

echo_cpp ()
{
    echo -e "//
//  $1.$2
//  $4
//
//  Created by $USERNAME on $DATE.
//

#include \"$1.$3\"\n\n"
}



echo -e "\n---> create placeholder files"
echo "----> idl/$1.idl"
echo -e "\
# $1.idl \n\
# \n\
#   definitions for $1 native extensions \n\
\n\
$1 = interface +j +o { \n\
\tlog_string(str: string): bool; \n\
}" > "$dest_dir/idl/$1.idl"

echo "----> src/cpp/$1_impl.hpp"
echo_hpp "$1_impl" "hpp" $current_project_name > "$dest_dir/src/cpp/$1.hpp"

echo "----> src/cpp/$1_impl.cpp"
echo_cpp "$1_impl" "cpp" "hpp" $current_project_name > "$dest_dir/src/cpp/$1.cpp"


echo "----> src/objc/Ctw$1.h"
echo_hpp "Ctw$1" "h" $current_project_name > "$dest_dir/src/objc/$1.h"

echo "----> src/objc/Ctw$1.m"
echo_cpp "Ctw$1" "m" "h" $current_project_name > "$dest_dir/src/objc/$1.m"


echo "----> test/$1_test.hpp"
echo_hpp "$1_test" "hpp" $current_project_name > "$dest_dir/test/$1_test.hpp"

echo "----> test/$1_test.cpp"
echo_cpp "$1_test" "cpp" $current_project_name > "$dest_dir/test/$1_test.cpp"





echo -e "\n---> updating Makefile"

DJINNI_COMMENT_TXT="# - Djinni generated-src \(extensions\)"
DJINNI_MAKE="\n./$1/generated-src: djinni \n\t./script/build/idl.sh build $1/idl $1/generated-src"
awk "
!/build_ios\/$current_project_name.xcodeproj:/   { print; } 
/$DJINNI_COMMENT_TXT/                            { print \"$DJINNI_MAKE\" }
/build_ios\/$current_project_name.xcodeproj:/    { print \$0, \"./$1/generated-src\"  }
/clean:/                                         { print \"\t./script/build/idl.sh clean $1/generated-src\" }
" \
    Makefile > .Makefile.tmp
mv .Makefile.tmp Makefile



echo -e "\n---> updating $current_project_name.gyp"

TARGETS_TXT="'targets':"
GYP_TARGET_DEF="\t# $1 extension CPP target \n \
    \{ \n \
      'target_name': 'lib$1', \n \
      'type': 'static_library', \n \
      'sources': [ \n \
        '<!@(python script/build/glob.py $1/src/cpp/ *.h *.hpp *.cpp)', \n \
        '<!@(python script/build/glob.py $1/generated-src/cpp/ *.h *.hpp *.cpp)', \n \
        '<!@(python script/build/glob.py $1/test/ *.h *.hpp *.cpp)', \n \
      ], \n \
      'include_dirs': [ \n \
        '$1/src/cpp', \n \
        '$1/generated-src/cpp', \n \
        '$1/test', \n \
      ], \n \
      'all_dependent_settings': \{ \n \
        'include_dirs': [ \n \
          '$1/src/cpp', \n \
          '$1/generated-src/cpp', \n \
          '$1/test', \n \
        ], \n \
        'libraries': [ \n \
          'libc++.a', \n \
        ], \n \
      \}, \n \
    \},"

OBJC_INCLUDES_DEF="'<!@(python script/build/glob.py $1/generated-src/objc/ *.h *.hpp *.mm *.m)', \
                 \n'<!@(python script/build/glob.py $1/src/objc/ *.h *.m *.mm *.hpp *.cpp)',"

APP_TARGET_TXT="'target_name': '$current_project_name',"
DEPENDENCIES_TXT="'dependencies':"
SOURCES_TXT="'sources':"
INCLUDE_DIRS_TXT="'include_dirs':"

awk \
" BEGIN{ TARGET_LINE=999 }
                    { print; }
/$TARGETS_TXT/      { print \"\n$GYP_TARGET_DEF\" }
/$DEPENDENCIES_TXT/ { if(NR > TARGET_LINE) { print \"'lib$1',\" } }
/$SOURCES_TXT/      { if(NR > TARGET_LINE) { print \"$OBJC_INCLUDES_DEF\" } }
/$DEPENDENCIES_TXT/ { if(NR > TARGET_LINE) { print \"\" } }
/$APP_TARGET_TXT/   { TARGET_LINE = NR }" \
"$current_project_name.gyp" > ".$current_project_name.gyp.tmp"
mv ".$current_project_name.gyp.tmp" "$current_project_name.gyp"
make pretty_gyp 2>&1 >/dev/null




echo -e "\n---> updating .gitignore"

GENERATED_SRC_TXT="# Djinni generated sources"
awk "
                        { print; }
/$GENERATED_SRC_TXT/    { print \"$1/generated-src/\" }
" \
    .gitignore > .gitignore.tmp
mv .gitignore.tmp .gitignore



# ---- commit new files to local Git repo
echo -e "\n--> committing changes to Git"
git add Makefile "$current_project_name.gyp" $1 .gitignore
git commit -m "C-the-World new extension scaffolding added: $1/" 2>&1 >/dev/null


echo -e "\n--> done scaffolding for $1/ <--"
