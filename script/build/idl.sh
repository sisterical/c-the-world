#! /usr/bin/env bash

script_dir=$(dirname $(realpath "$0"))
base_dir=$(cd "`dirname "0"`" && pwd)


print_usage ()
{
    SN=$(basename "$1")
    echo ""
    echo -e "-C-the-World-\tbuild & clean generated sources from Djinni IDL"
    echo "Usage: $SN [build|clean] [PATH/TO/IDL/FILES] OUTPUT/PATH"
    echo "example: $SN build ctheworld/idl ctheworld/generated-src"
    echo "example: $SN clean ctheworld/generated-src"
}


# validate arguments
case "$1" in
    build)
        if (( $# != 3 )); then
            print_usage "$0"
            exit 0
        fi
        src_path=$2
        dest_path=$3
        ;;
    clean)
        if (( $# != 2 )); then
            print_usage "$0"
            exit 0
        fi
        dest_path=$2
        ;;
    *)
        print_usage "$0"
        exit 0
        ;;
esac



# output directories for Djinni generated sources
gen_dir="$base_dir/$dest_path"
cpp_out="$gen_dir/cpp"
objc_out="$gen_dir/objc"
jni_out="$gen_dir/jni"
java_dir=$(echo $JAVA_PACKAGE | tr . /)
java_out="$gen_dir/java/$java_dir"




# load configuration
if [ $1 == "build" ]
then
    idl_path="$base_dir/$src_path"

    if [[ ! -d $idl_path ]]; then
        echo -e "ERROR: $src_path does not exist.\n"
        print_usage "$0"
        exit 0
    fi

    # load build configuration defaults
    source "$script_dir/config.defaults"

    # load configuration overloads
    if [[ ! -f "$idl_path/build.config" ]]
    then
        echo "Missing override configuration: $src_path/build.config"
    else
        source "$idl_path/build.config"
    fi


    # $CPP_NAMESPACE
    #  if $CPP_NAMESPACE hasn't been set in config, then use 
    #  [first_dirname_in_the_path]_gen
    if [ -z ${CPP_NAMESPACE+x} ]; then
        CPP_NAMESPACE=$( echo $2 | awk -F '/' '{ print $1, "_gen" }' | tr -d ' ' )
    fi


    # execute the djinni command
    for file in $(find $idl_path -type f -name "*.idl" -depth 1)
    do
        echo "building IDL from ${file}"
        $base_dir/deps/djinni/src/run \
            --java-out $java_out \
            --java-package $JAVA_PACKAGE \
            --ident-java-field mFooBar \
            --cpp-out $cpp_out \
            --cpp-namespace $CPP_NAMESPACE \
            --jni-out $jni_out \
            --ident-jni-class NativeFooBar \
            --ident-jni-file NativeFooBar \
            --objc-out $objc_out \
            --objc-type-prefix $OBJC_PREFIX \
            --objcpp-out $objc_out \
            --cpp-optional-template std::experimental::optional \
            --cpp-optional-header "<experimental/optional>" \
            --idl $file
    done
else
    # clean generated sources from dirs
    rm -rf $cpp_out
    rm -rf $jni_out
    rm -rf $objc_out
    rm -rf $java_out
    rm -rf $dest_path
fi
