#!/usr/bin/env python
# a helper for gyp to do file globbing cross platform
# Usage: python glob.py root_dir pattern [pattern ...]

import fnmatch
import os
import sys
from argparse import ArgumentParser


# parse command line arguments
parser = ArgumentParser()
parser.add_argument('root_dir', help='root filesystem directory to begin search')
parser.add_argument("-r", "--restrict-dirs", dest="dir_pattern",
                    help="pattern for directories to restrict file pattern search")
parser.add_argument("-i", "--include-dirs", dest="include_dirs", default=False, action='store_true',
                    help="include directory names in the match results")
parser.add_argument('pattern', nargs='*',
                    help='pattern(s) for files (or directories) to match')
args = vars(parser.parse_args())




def filterStringsWithPatterns(strs, patterns, prepend=""):
    # match patterns to strs. return matches
    matched = []
    
    for s in strs:
        if (len(patterns) == 0):
            matched.append(prepend + s)
            
        else:
            match = False
            for p in patterns:
                match = match or fnmatch.fnmatch(s, p)
            if match:
                matched.append(prepend + s)
    return matched



# match file [patterns] with an os.walk down the [root_path]
def matchFilePatterns(root_path, include_dirs, patterns):
    matched = []
    for (dirpath, dirnames, files) in os.walk(root_path):
        matched.extend( filterStringsWithPatterns(files, patterns, dirpath + '/') )

        if (include_dirs):
            matched.extend( filterStringsWithPatterns(dirnames, patterns, dirpath + '/') )
    return matched



if (args["dir_pattern"] == None):
    # when --dirs arg isn't given; pattern match all file names from [root_dir]
    paths = matchFilePatterns(args["root_dir"], args["include_dirs"], args["pattern"])
    for p in paths:
        print p
else:
    # when --dirs arg isn't given; find those subdirs and pattern match files below them
    dirpaths = []

    # build dirpaths
    for (dirpath, dirnames, _) in os.walk(args["root_dir"]):
        for d in dirnames:
            if fnmatch.fnmatch(d, args["dir_pattern"]):
                dirpaths.append(dirpath + '/' + d)

    # match dirpaths            
    for dp in dirpaths:
        paths = matchFilePatterns(dp, args["include_dirs"], args["pattern"])
        for p in paths:
            print p
